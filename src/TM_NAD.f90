!--------------------------------------------------------------------------------------------------
! PROGRAM: NADInterface
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date May, 2017
!
!--------------------------------------------------------------------------------------------------
program nadinterface

    use fortutils
    use stringmod
    use filestype
    use tmoptstype
    use qminfotype

    use ccgmod
    use oneelopmod
    use mooverlapmod
    use cisoverlapmod
    use write_txt_mod

    use nad_interface

    use turbomolereadmod
    use turbomolewritemod
    use turbomolerunmod
    implicit none
    
    ! Directory names.
    character(len=:), allocatable :: pwd
    character(len=:), allocatable :: dir2
    character(len=8), parameter :: dir1 = 'prevstep'
    ! Current geometry variables.
    type(files) :: fn2
    type(tmopts) :: opt2
    type(qminfo) :: qm2
    ! Previous geometry variables.
    type(files) :: fn1
    type(tmopts) :: opt1
    type(qminfo) :: qm1
    ! Overlap variables.
    real(dp), allocatable :: smat(:,:)
    real(dp), allocatable :: cscmat(:,:,:)
    real(dp), allocatable :: tcscmat(:,:,:)
    real(dp), allocatable :: omat(:, :)
    ! QMMM variables
    integer :: pc_n !< Number of point charges.
    real(dp), allocatable :: pc_geo(:, :) !< Locations of point charges.
    ! Helper variables.
    character(len=1000) :: temp
    integer :: cstate
    integer :: cexstate
    logical :: newgeom
    logical :: reqolap
    logical :: reqosci
    logical :: pcharge
    logical :: check
    integer :: iunit
    integer :: outunit
    integer :: i, j, b,s 

    !--------------------------------------------INPUT--------------------------------------------
    ! Get name of initial directory.
    call getcwd(temp)
    pwd = trim(adjustl(temp))

    ! Read status file.
    call nad_interface_read_status(cstate = cstate, newgeom = newgeom, reqcoup = reqolap,          &
    &                              reqosci = reqosci, pcharge = pcharge)
    cexstate = cstate - 1

    ! Read new geometry.
    if (newgeom) then
        call nad_interface_read_atoms(qnat = qm2%nat, qsym = qm2%asym, qgeo = qm2%axyz,            &
        &                             mnat = pc_n, mgeo = pc_geo)
    end if

    ! Get name of QM directory.
    dir2 = 'qmdir'
    call get_environment_variable('QMDIR', temp)
    if (temp /= '') dir2 = trim(adjustl(temp))
    call system('mkdir -p '//dir2)

    ! Enter QM direcotry.
    call chdir(dir2)
    !-----------------------------------------CALCULATION------------------------------------------
    ! Read previous step.
    if (reqolap) then
        call chdir(dir1)
        call turboreadall(fn1, opt1, qm1, 0)
        call chdir('..')
    end if
    
    ! Update control file.
    call turboreadopts(fn2, opt2)
    call turbowritecontrolgrad(fn2, opt2, cexstate)
    if (newgeom) then
        ! Update coord file.
        call turbowritecoord(fn2%fn('coord'), qm2%axyz, qm2%asym)
        if (pcharge) call turbowritepcxyz(fn2, pc_geo)
        ! Run SCF.
        call turborunscf(opt2, 'qm.out', 'qm.err')
    end if
    ! Run excited state/gradient calculation.
    call turborunex(opt2, cexstate, 'qm.out', 'qm.err')
    call turboreadall(fn2, opt2, qm2, 0) ! Read results.

    if (reqolap) then
      ! ! Calculate AO overlaps.
      ! call oneelop(qm1%ao_c, qm2%ao_c, [0,0,0], qm1%ao_c2s, qm2%ao_c2s, smat)
      ! ! New output.
      ! call write_txt('mo1', qm1%mo_c(:,qm1%nmo_fo(1)+1:,:))
      ! call write_txt('mo2', qm2%mo_c(:,qm1%nmo_fo(1)+1:,:))
      ! 
      ! call write_txt('wfa1', reshape(qm1%wf(1)%c, [qm1%nmo_av(1), qm1%nmo_ao(1), qm1%nexst]))
      ! call write_txt('wfa2', reshape(qm2%wf(1)%c, [qm2%nmo_av(1), qm2%nmo_ao(1), qm2%nexst]))
      ! 
      ! ! Calculate molecular orbital overlaps.
      ! call oneelop(qm1%ao_c, qm2%ao_c, [0,0,0], qm1%ao_c2s, qm2%ao_c2s, smat)
      ! call write_txt('s_ao', smat)

        call system('../overlap.exe --print-level 0 --orth-overlap --orth-states prevstep .')

        ! Calculate MO overlaps.
   !    call mooverlap(qm1%mo_c, qm2%mo_c, smat, tcscmat)
   !    call reducecsc(qm1%mo_omask, qm2%mo_omask, qm1%mo_amask, qm2%mo_amask, tcscmat, cscmat)

   !    open(newunit=outunit, file='cscmat')
   !    write(outunit, *) qm1%rhf, qm1%nmo_a, qm2%nmo_a
   !    do b = 1, qm1%rhf
   !        do i = 1, qm2%nmo_a
   !            write(outunit, '(10000(x,e23.17))') cscmat(:, i, b)
   !        end do
   !    end do
   !    close(outunit)
   !    open(newunit=outunit, file='wf1')
   !    write(outunit, '(2(x,i0))') qm1%nexst, qm1%rhf
   !    do i = 1, qm1%rhf
   !        write(outunit, '(2(x,i0))') qm1%nmo_ao(i), qm1%nmo_av(i)
   !    end do
   !    do i = 1, qm1%nexst
   !        write(outunit, '(10000(x,e23.17))') qm1%exen(i)
   !        do j = 1, qm1%rhf
   !            do b = 1, qm1%nmo_ao(j)
   !                write(outunit, '(10000(x,e23.17))') qm1%wf(j)%c((b-1)*qm1%nmo_av(j)+1:b*qm1%nmo_av(j) ,i)
   !            end do
   !        end do
   !    end do
   !    close(outunit)
   !    open(newunit=outunit, file='wf2')
   !    write(outunit, '(2(x,i0))') qm2%nexst, qm2%rhf
   !    do i = 1, qm2%rhf
   !        write(outunit, '(2(x,i0))') qm2%nmo_ao(i), qm2%nmo_av(i)
   !    end do
   !    do i = 1, qm2%nexst
   !        write(outunit, '(10000(x,e23.17))') qm2%exen(i)
   !        do j = 1, qm2%rhf
   !            do b = 1, qm2%nmo_ao(j)
   !                write(outunit, '(10000(x,e23.17))') qm2%wf(j)%c((b-1)*qm2%nmo_av(j)+1:b*qm2%nmo_av(j) ,i)
   !            end do
   !        end do
   !    end do
   !    close(outunit)
   !    call system('../overlap.exe omat > /dev/null 2>&1')
        allocate(omat(qm1%nst, qm2%nst))
        open(newunit=outunit, file='omat', status='old')
        do i =1, qm2%nst
            read(outunit, *) omat(i, :)
        end do
        close(outunit)
        call system('rm omat')
    end if


    !--------------------------------------------OUTPUT--------------------------------------------
    ! Overwrite the previous step with the current step.
    call system('rm -rf '//dir1)
    call system('mkdir -p '//dir1)
    call system('cp * '//dir1//' 2> /dev/null')

    ! Return to initial directory.
    call chdir(pwd)

    call nad_interface_write_qm(reqolap, reqosci, qm2%en, qm2%agrd, os = qm2%exos, olap = omat)


end program nadinterface
