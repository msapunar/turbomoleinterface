!--------------------------------------------------------------------------------------------------
! PROGRAM: NADInterface
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date May, 2017
!
!--------------------------------------------------------------------------------------------------
program tm_diaopt

    use fortutils
    use stringmod
    use filestype
    use tmoptstype
    use qminfotype

    use ccgmod
    use oneelopmod
    use mooverlapmod

    use assignprobmod

    use turbomolereadmod
    use turbomolewritemod
    use turbomolerunmod
    implicit none
    
    character(len=:), allocatable :: pwd
    type(files) :: fn
    type(tmopts) :: opt
    type(qminfo) :: qm1
    type(qminfo) :: qm2
    ! Overlap variables.
    real(dp), allocatable :: smat(:,:)
    real(dp), allocatable :: cscmat(:,:,:)
    real(dp), allocatable :: tcscmat(:,:,:)
    real(dp), allocatable :: omat(:, :)
    ! Helper variables.
    character(len=1000) :: temp
    integer :: cstate, pstate
    integer :: cexstate
    integer :: outunit
    integer :: i
    integer :: maxnstep = 20
    real(dp) :: descent_factor = 1._dp
    integer, allocatable :: row2col(:)

    !--------------------------------------------INPUT--------------------------------------------
    ! Get name of initial directory.
    call getcwd(temp)
    pwd = trim(adjustl(temp))

    
    cstate = 6
    cexstate = 5
    call turboreadopts(fn, opt)
    call turbowritecontrolgrad(fn, opt, cexstate)

    do i = 0, maxnstep
        call turborunscf(opt, 'qm.out', 'qm.err')
        call turborunex(opt, cexstate, 'qm.out', 'qm.err')
        call turboreadall(fn, opt, qm2, 0) ! Read results.

        if (i > 0) then
            call get_olap()

            omat = -omat**2
            call assignprob(qm1%nst, omat, row2col)
            
            if (row2col(cstate) /= cstate) then
                pstate = cstate
                cstate = row2col(cstate)
                cexstate = cstate - 1
                write(stdout, *) 'Change of state detected, previous state: ', pstate, 'new state:', cstate
                call turbowritecontrolgrad(fn, opt, cexstate)
            end if
        else
            allocate(row2col(qm2%nst))
        end if
        write(temp, '(a,i0.3)') 'step_',i
        call system('mkdir '//trim(temp))
        call system('cp * '//trim(temp)//' 2> /dev/null')
        call turboreadall(fn, opt, qm1, 0) ! Read results.

        qm2%axyz = qm2%axyz - descent_factor * qm2%agrd
        call turbowritecoord(fn%fn('coord'), qm2%axyz, qm2%asym)
    end do



contains

    subroutine get_olap()
        integer :: b, i, j
        call oneelop(qm1%ao_c, qm2%ao_c, [0,0,0], qm1%ao_c2s, qm2%ao_c2s, smat)
        call mooverlap(qm1%mo_c, qm2%mo_c, smat, tcscmat)
        call reducecsc(qm1%mo_omask, qm2%mo_omask, qm1%mo_amask, qm2%mo_amask, tcscmat, cscmat)
        open(newunit=outunit, file='cscmat')
        write(outunit, *) qm1%rhf, qm1%nmo_a, qm2%nmo_a
        do b = 1, qm1%rhf
            do i = 1, qm2%nmo_a
                write(outunit, '(10000(1x,e23.17))') cscmat(:, i, b)
            end do
        end do
        close(outunit)
        open(newunit=outunit, file='wf1')
        write(outunit, '(2(1x,i0))') qm1%nexst, qm1%rhf
        do i = 1, qm1%rhf
            write(outunit, '(2(1x,i0))') qm1%nmo_ao(i), qm1%nmo_av(i)
        end do
        do i = 1, qm1%nexst
            write(outunit, '(10000(1x,e23.17))') qm1%exen(i)
            do j = 1, qm1%rhf
                do b = 1, qm1%nmo_ao(j)
                    write(outunit, '(10000(1x,e23.17))') qm1%wf(j)%c((b-1)*qm1%nmo_av(j)+1:b*qm1%nmo_av(j) ,i)
                end do
            end do
        end do
        close(outunit)
        open(newunit=outunit, file='wf2')
        write(outunit, '(2(1x,i0))') qm2%nexst, qm2%rhf
        do i = 1, qm2%rhf
            write(outunit, '(2(1x,i0))') qm2%nmo_ao(i), qm2%nmo_av(i)
        end do
        do i = 1, qm2%nexst
            write(outunit, '(10000(1x,e23.17))') qm2%exen(i)
            do j = 1, qm2%rhf
                do b = 1, qm2%nmo_ao(j)
                    write(outunit, '(10000(1x,e23.17))') qm2%wf(j)%c((b-1)*qm2%nmo_av(j)+1:b*qm2%nmo_av(j) ,i)
                end do
            end do
        end do
        close(outunit)
        call system('overlap.exe omat > /dev/null 2>&1')
        if (.not. allocated(omat)) allocate(omat(qm1%nst, qm2%nst))
        open(newunit=outunit, file='omat', status='old')
        do i =1, qm2%nst
            read(outunit, *) omat(:, i)
        end do
        close(outunit)
     end subroutine get_olap


end program tm_diaopt
