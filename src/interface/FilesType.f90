MODULE FilesType
    use fortutils
    use stringmod
    IMPLICIT NONE
 
    PRIVATE
 
    PUBLIC :: Files
 
 
    !----------------------------------------------------------------------------------------------
    ! TYPE: FileName
    !
    ! DESCRIPTION:
    !> @brief 
    !! Helper type for the Files type.
    !> @details 
    !! Contains the name of a single file, a key by which the file can be found and an optional
    !! index that can be used if a key is to be used multiple times.
    !----------------------------------------------------------------------------------------------
    TYPE FileName
        character(len=:), allocatable :: Key
        character(len=:), allocatable :: FN
        integer :: Indx
    END TYPE FileName
 
    !----------------------------------------------------------------------------------------------
    ! TYPE: Files
    !
    ! DESCRIPTION:
    !> @brief 
    !! Data type for storing and accessing files in a directory.
    !> @details 
    !! Contains a list of files, procedures for adding new members and returning names from the
    !! list.
    !----------------------------------------------------------------------------------------------
    TYPE Files
        character(len=:), allocatable :: Dir
        integer :: NFile = 0
        type(FileName), allocatable :: List(:)
    contains
        procedure :: Q => Files_Q
        procedure :: Add => Files_Add
        procedure :: ReadFN => Files_ReadFN
        procedure :: FN => Files_FN
    END TYPE Files
 
 
CONTAINS

    !----------------------------------------------------------------------------------------------
    ! FUNCTION: Files_Q
    !
    ! DESCRIPTION:
    !> @brief 
    !! Returns the position of a file with a given keyword/index in the list. Returns 0 if the file
    !! is not present in the list.
    !----------------------------------------------------------------------------------------------
    FUNCTION Files_Q(Self, Key, Indx) RESULT(pos)
        class(Files), intent(in) :: Self
        character(len=*), intent(in) :: Key
        integer, intent(in) :: Indx
        integer :: pos
        integer :: i
      
        pos = 0
        DO i = 1, self%NFile
            IF (Self%List(i)%Key /= Key) CYCLE
            IF (Self%List(i)%Indx /= Indx) CYCLE
            pos = i
            RETURN
        END DO
    END FUNCTION Files_Q


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: Files_Add
    !
    ! DESCRIPTION:
    !> @brief Adds a file to the list of files in Self%List.
    !> @details
    !! If a file with the same key (and index) is already present in the list, it is replaced.
    !! The size of the list is adjusted automatically to fit as many files as needed.
    !----------------------------------------------------------------------------------------------
    SUBROUTINE Files_Add(Self, Key, FN, Indx)
        class(Files), intent(inout) :: Self
        character(len=*), intent(in) :: Key
        character(len=*), intent(in) :: FN
        integer, intent(in), optional :: Indx
        
        character(len=:), allocatable :: tmpkey
        character(len=:), allocatable :: tmpfn
        integer :: tmpindx
        integer :: pos
        integer :: csize
        integer, parameter :: alloc = 20
        type(FileName), allocatable :: TempList(:)
        
        tmpkey = trim(adjustl(key))
        tmpfn = trim(adjustl(fn))
        tmpindx = 0
        IF (present(Indx)) tmpindx = Indx
        
        IF (allocated(Self%List)) THEN
            pos = self%Q(tmpkey, tmpindx)
        else
            ! On first call allocate the list to default size.
            allocate(Self%List(alloc))
            pos = 0
        END IF
        IF (.not. allocated(Self%Dir)) Self%Dir = ''
        
        IF (pos == 0) THEN
            csize = size(Self%List)
            IF (Self%NFile == csize) THEN
                ! Automatically increase the list if it becomes full.
                allocate(TempList(csize + alloc))
                TempList(1:csize) = Self%List
                call move_alloc(templist, self%list)
            END IF
            self%NFile = Self%NFile + 1
            self%List(Self%NFile) = FileName(tmpkey, tmpfn, tmpindx)
        else
            self%List(pos) = FileName(tmpkey, tmpfn, tmpindx)
        END IF
    END SUBROUTINE Files_Add
 
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: Files_ReadFN
    !
    ! DESCRIPTION:
    !> @brief 
    !! Read a file name (FN) from a line and add it to the list using Files_Add.
    !> @details
    !! The line should contain a keyword (file/fname) followed by whitespace or equal sign followed
    !! by the name of the file.
    !!   Example: '... file = FN ...'
    !----------------------------------------------------------------------------------------------
    SUBROUTINE Files_ReadFN(Self, Line, Key, Indx, DefaultFN)
       class(files), intent(inout) :: Self
       type(Parser) :: Line
       character(len=*), intent(in) :: Key
       integer, intent(in), optional :: Indx
       character(len=*), intent(in), optional :: DefaultFN
       
       integer :: tmpindx
       integer :: i
       
       tmpindx = 0
       IF (present(Indx)) tmpindx = Indx
       call Line%Parse('= ')
       IF (Line%NArg < 2) GoTo 199 
 
       DO i = 1, Line%NArg
          IF (Line%Args(i) == 'file') GoTo 109
          IF (Line%Args(i) == 'fname') GoTo 109
          CYCLE
          109 IF (Line%NArg < i + 1) THEN
             write(stderr,*)
             write(stderr,*) 'Subroutine Files_ReadFN failed to read line, &
                              &no filename after keyword'
             write(stderr,*) 'Line: ', Line%Str
             STOP 'Error in FileType module. Files_ReadFN subroutine.'
          END IF
          call Self%Add(Key, trim(Line%Args(i+1)), tmpindx)
          RETURN
       END DO
       
       199 IF (present(DefaultFN)) THEN
          call Self%Add(Key, DefaultFN, tmpindx)
          RETURN
       else
          write(stderr,*)
          write(stderr,*) 'Subroutine Files_ReadFN failed to read line, &
                           &keyword not found and default name not given.'
          write(stderr,*) 'Line: ', Line%Str
          STOP 'Error in FileType module. Files_ReadFN subroutine.'
       END IF
 
    END SUBROUTINE Files_ReadFN
 
 
    !----------------------------------------------------------------------------------------------
    ! FUNCTION: Files_FN
    !
    ! DESCRIPTION:
    !> @brief 
    !! Return the full path to the requested file.
    !----------------------------------------------------------------------------------------------
    FUNCTION Files_FN(Self, Key, Indx) RESULT(FN)
        class(files), intent(in) :: self
        character(len=*), intent(in) :: key
        integer, intent(in), optional :: indx
        character(len=:), allocatable :: fn
        
        character(len=:), allocatable :: tmpkey
        integer :: tmpindx
        integer :: pos
        
        tmpkey = trim(adjustl(key))
        tmpindx = 0
        IF (present(indx)) tmpindx = indx
        
        pos = self%Q(tmpkey, tmpindx)
        
        IF (pos /= 0) THEN
            fn = self%dir // self%list(pos)%fn
        else
            write(stderr,*)
            write(stderr,*) 'Subroutine Files_FN failed with input:'
            write(stderr,*) 'Keyword: ', key
            IF (present(indx)) write(stderr,*) 'Index: ', indx
            write(stderr,*)
            STOP 'Error in FileType module. Files_FN subroutine.'
        END IF
    END FUNCTION Files_FN


END MODULE FilesType
