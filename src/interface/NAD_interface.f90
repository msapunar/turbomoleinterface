module nad_interface
    use fortutils
    implicit none

    private
    public nad_interface_read_status
    public nad_interface_read_atoms
    public nad_interface_read_qm
    public nad_interface_read_mm
    public nad_interface_write_status
    public nad_interface_write_atoms
    public nad_interface_write_qm
    public nad_interface_write_mm

    character(len=10), parameter :: f_status = 'nad_status'
    character(len=9), parameter :: f_atoms = 'nad_atoms'
    character(len=4), parameter :: f_qmen = 'qmen'
    character(len=6), parameter :: f_qmgrad = 'qmgrad'
    character(len=6), parameter :: f_qmolap = 'qmolap'
    character(len=8), parameter :: f_qmnadv = 'qmnadvec'
    character(len=4), parameter :: f_mmen = 'mmen'
    character(len=6), parameter :: f_mmgrad = 'mmgrad'
    character(len=3), parameter :: f_pbc = 'pbc'


contains


    subroutine nad_interface_write_status(step, cstate, newgeom, reqcoup, reqosci, pcharge)
        integer, intent(in) :: step
        integer, intent(in) :: cstate
        logical, intent(in) :: newgeom
        logical, intent(in) :: reqcoup
        logical, intent(in) :: reqosci
        logical, intent(in) :: pcharge
        integer :: iunit

        open(newunit=iunit, file=f_status, action='write')
        write(iunit, *) step
        write(iunit, *) cstate
        write(iunit, *) newgeom
        write(iunit, *) reqcoup
        write(iunit, *) reqosci
        write(iunit, *) pcharge
        close(iunit)
    end subroutine nad_interface_write_status


    subroutine nad_interface_read_status(step, cstate, newgeom, reqcoup, reqosci, pcharge)
        integer, intent(out), optional :: step
        integer, intent(out), optional :: cstate
        logical, intent(out), optional :: newgeom
        logical, intent(out), optional :: reqcoup
        logical, intent(out), optional :: reqosci
        logical, intent(out), optional :: pcharge
        integer :: tstep
        integer :: tcstate
        logical :: tnewgeom
        logical :: treqcoup
        logical :: treqosci
        logical :: tpcharge
        integer :: iunit

        open(newunit=iunit, file=f_status, action='read')
        read(iunit, *) tstep
        read(iunit, *) tcstate
        read(iunit, *) tnewgeom
        read(iunit, *) treqcoup
        read(iunit, *) treqosci
        read(iunit, *) tpcharge
        close(iunit)

        if (present(step)) step = tstep   
        if (present(cstate)) cstate = tcstate 
        if (present(newgeom)) newgeom = tnewgeom
        if (present(reqcoup)) reqcoup = treqcoup
        if (present(reqosci)) reqosci = treqosci
        if (present(pcharge)) pcharge = tpcharge
    end subroutine nad_interface_read_status


    subroutine nad_interface_write_atoms(nat, qnat, mnat, qind, mind, sym, geo, vel, pbc, box)
        integer, intent(in) :: nat, qnat, mnat
        integer, intent(in) :: qind(:), mind(:)
        character(len=2), intent(in) :: sym(:)
        real(dp), intent(in) :: geo(:, :), vel(:, :)
        logical, intent(in) :: pbc
        real(dp), intent(in) :: box(6)
        character(len=21), parameter :: atomfmt = '(1x,a2,2x,1000e24.16)'
        character(len=17), parameter :: relfmt = '(1000(e24.16,2x))'
        integer :: ounit, i

        open(newunit=ounit, file=f_atoms, action='write')
        write(ounit, *) nat, qnat, mnat
        call writeintlist(ounit, qnat, qind)
        call writeintlist(ounit, mnat, mind)
        do i = 1, nat
            write(ounit, atomfmt) sym(i), geo(:, i), vel(:, i)
        end do
        if (pbc) write(ounit, relfmt) box
        close(ounit)
    end subroutine nad_interface_write_atoms


    subroutine nad_interface_read_atoms(nat, qnat, mnat, sym, qsym, msym, geo, qgeo, mgeo, vel,    &
    &                             qvel, mvel, pbc, box)
        integer, intent(out), optional :: nat, qnat, mnat
        character(len=2), allocatable, dimension(:), intent(out), optional :: sym, qsym, msym
        real(dp), allocatable, dimension(:, :), intent(out), optional :: geo, qgeo, mgeo
        real(dp), allocatable, dimension(:, :), intent(out), optional :: vel, qvel, mvel
        logical, intent(out), optional :: pbc
        real(dp), intent(out), optional :: box(6)
        integer :: n, n1, n2
        character(len=2), allocatable :: tsym(:)
        real(dp), allocatable :: tgeo(:, :)
        real(dp), allocatable :: tvel(:, :)
        integer, allocatable :: ql(:)
        integer, allocatable :: ml(:)
        logical :: flag_pbc
        real(dp) :: tbox(6)
        integer :: iunit, i, err

        open(newunit=iunit, file=f_atoms, action='read')
        ! Read number of atoms and allocate.
        read(iunit, *) n, n1, n2
        allocate(tsym(n))
        allocate(tgeo(3, n))
        allocate(tvel(3, n))
        allocate(ql(n1))
        allocate(ml(n2))
        ! Read index lists.
        read(iunit, *) ql
        read(iunit, *) ml
        ! Read geometry and velocity
        do i = 1, n
            read(iunit, *) tsym(i), tgeo(:, i), tvel(:, i)
        end do

        ! Read box information.
        flag_pbc = .true.
        read(iunit, *, iostat=err) tbox
        if (err < 0) then
            flag_pbc = .false.
            tbox = 0
        end if
        close(iunit)

        if (present(nat)) nat = n
        if (present(qnat)) qnat = n1
        if (present(mnat)) mnat = n2
        if (present(sym)) allocate(sym(1:n), source = tsym)
        if (present(geo)) allocate(geo(3, 1:n), source = tgeo)
        if (present(vel)) allocate(vel(3, 1:n), source = tvel)
        if (present(qsym)) allocate(qsym(1:n1), source = tsym(ql))
        if (present(qgeo)) allocate(qgeo(3, 1:n1), source = tgeo(:, ql))
        if (present(qvel)) allocate(qvel(3, 1:n1), source = tvel(:, ql))
        if (present(msym)) allocate(msym(1:n2), source = tsym(ml))
        if (present(mgeo)) allocate(mgeo(3, 1:n2), source = tgeo(:, ml))
        if (present(mvel)) allocate(mvel(3, 1:n2), source = tvel(:, ml))
        if (present(pbc)) pbc = flag_pbc
        if (present(box)) box = tbox
    end subroutine nad_interface_read_atoms


    subroutine nad_interface_read_qm(reqcoup, reqosci, tdc_type, en, qind, grad, os, olap, nadv)
        logical, intent(in) :: reqcoup
        logical, intent(in) :: reqosci
        integer, intent(in) :: tdc_type
        real(dp), intent(out) :: en(:)
        integer, intent(in) :: qind(:)
        real(dp), intent(out) :: grad(:, :)
        real(dp), intent(out) :: os(:)
        real(dp), intent(out) :: olap(:, :)
        real(dp), intent(out) :: nadv(:, :, :)

        integer :: iunit, i, j

        call needfile(f_qmen)
        open(newunit=iunit, file=f_qmen, action='read')
        read(iunit, *) en
        if (reqosci) read(iunit, *) os
        close(iunit)

        call needfile(f_qmgrad)
        open(newunit=iunit, file=f_qmgrad, action='read')
        do i = 1, size(qind)
            read(iunit, *) grad(:, qind(i))
        end do
        close(iunit)

        if (reqcoup) then
            select case(tdc_type)
            case(1)
                call needfile(f_qmolap)
                open(newunit=iunit, file=f_qmolap, action='read')
                do i = 1, size(olap, 2)
                    read(iunit, *) olap(:, i)
                end do
                close(iunit)
            case(2)
                call needfile(f_qmnadv)
                open(newunit=iunit, file=f_qmnadv, action='read')
                do i = 1, size(nadv, 2)
                    do j = 1, size(nadv, 3)
                        read(iunit, *) nadv(:, i, j)
                    end do
                end do
                close(iunit)
            end select
        end if
    end subroutine nad_interface_read_qm


    subroutine nad_interface_write_qm(reqcoup, reqosci, en, grad, os, olap, nadvec)
        logical, intent(in) :: reqcoup
        logical, intent(in) :: reqosci
        real(dp), intent(in) :: en(:)
        real(dp), intent(in) :: grad(:, :)
        real(dp), intent(in), optional :: os(:)
        real(dp), intent(in), optional :: olap(:, :)
        real(dp), intent(in), optional :: nadvec(:, :, :)

        character(len=17), parameter :: relfmt = '(1000(e24.16,2x))'
        integer :: ounit, i, j

        open(newunit=ounit, file=f_qmen, action='write')
        write(ounit, relfmt) en
        if (reqosci) write(ounit, relfmt) os
        close(ounit)

        open(newunit=ounit, file=f_qmgrad, action='write')
        do i = 1, size(grad, 2)
            write(ounit, relfmt) grad(:, i)
        end do
        close(ounit)

        if (reqcoup) then
            if (present(olap)) then
                open(newunit=ounit, file=f_qmolap, action='write')
                do i = 1, size(olap, 2)
                    write(ounit, relfmt) olap(:, i)
                end do
                close(ounit)
            end if
            if (present(nadvec)) then
                open(newunit=ounit, file=f_qmnadv, action='read')
                do i = 1, size(nadvec, 2)
                    do j = 1, size(nadvec, 3)
                        write(ounit, relfmt) nadvec(:, i, j)
                    end do
                end do
                close(ounit)
            end if
        end if
    end subroutine nad_interface_write_qm


    subroutine nad_interface_read_mm(en, grad, qm_en, pbc, box)
        real(dp), intent(out) :: en
        real(dp), intent(out) :: grad(:, :)
        real(dp), intent(out) :: qm_en
        logical, intent(out) :: pbc
        real(dp), intent(out) :: box(6)
        integer :: iunit, i

        open(newunit=iunit, file=f_mmen, action='read')
        read(iunit, *) en, qm_en
        close(iunit)

        open(newunit=iunit, file=f_mmgrad, action='read')
        do i = 1, size(grad, 2)
            read(iunit, *) grad(:, i)
        end do
        close(iunit)

        if (pbc) then
            open(newunit=iunit, file=f_pbc, action='read')
            read(iunit, *) box
            close(iunit)
        end if
    end subroutine nad_interface_read_mm



    subroutine nad_interface_write_mm(en, grad, qm_en, pbc, box)
        real(dp), intent(in) :: en
        real(dp), intent(in) :: grad(:, :)
        real(dp), intent(in) :: qm_en
        logical, intent(in) :: pbc
        real(dp), intent(in) :: box(6)
        character(len=17), parameter :: relfmt = '(1000(e24.16,2x))'
        integer :: ounit, i

        open(newunit=ounit, file=f_mmen, action='write')
        write(ounit, relfmt) en, qm_en
        close(ounit)

        open(newunit=ounit, file=f_mmgrad, action='write')
        do i = 1, size(grad, 2)
            write(ounit, relfmt) grad(:, i)
        end do
        close(ounit)

        if (pbc) then
            open(newunit=ounit, file=f_pbc, action='write')
            write(ounit, relfmt) box
            close(ounit)
        end if
    end subroutine nad_interface_write_mm


    subroutine writeintlist(u, n, l)
        integer, intent(in) :: u
        integer, intent(in) :: n
        integer, intent(in) :: l(:)
        character(len=14), parameter :: ifmt = '(10000(1x,i0))'

        if (n > 0) then
            write(u, ifmt) l
        else
            write(u, *) 0
        end if
    end subroutine writeintlist

end module nad_interface
