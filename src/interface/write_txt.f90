module write_txt_mod
    use fortutils
    implicit none

    private
    public :: write_txt

    interface write_txt
        module procedure write_txt_dim2
        module procedure write_txt_dim3
    end interface write_txt

contains


    subroutine write_txt_dim2(fname, array)
        character(len=*), intent(in) :: fname
        real(dp), intent(in) :: array(:, :)
        integer :: i, ounit
        character(len=100) :: outfmt

        write(outfmt, '(a,i0,a)') '(', size(array, 1), 'es24.16)'

        open(newunit=ounit, file=fname, action='write')
        write(ounit, *) size(array, 1), size(array, 2)
        do i = 1, size(array, 2)
            write(ounit, outfmt) array(:, i)
        end do
        close(ounit)
    end subroutine write_txt_dim2


    subroutine write_txt_dim3(fname, array)
        character(len=*), intent(in) :: fname
        real(dp), intent(in) :: array(:, :, :)
        integer :: i, j, ounit
        character(len=100) :: outfmt

        write(outfmt, '(a,i0,a)') '(', size(array, 1), 'es24.16)'

        open(newunit=ounit, file=fname, action='write')
        write(ounit, *) size(array, 1), size(array, 2), size(array, 3)
        do i = 1, size(array, 3)
            do j = 1, size(array, 2)
                write(ounit, outfmt) array(:, j, i)
            end do
        end do
        close(ounit)
    end subroutine write_txt_dim3


end module write_txt_mod
