!--------------------------------------------------------------------------------------------------
! MODULE: TurbomoleRunMod
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date May, 2017
!--------------------------------------------------------------------------------------------------
module turbomolerunmod
    ! Import variables
    use fortutils
    use stringmod
    implicit none

    private
    public :: turborunscf
    public :: turborunex

contains

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboRunScf
    !----------------------------------------------------------------------------------------------
    subroutine turborunscf(opts, stdoutfile, stderrfile)
        ! Import classes
        use tmoptstype
        type(tmopts), intent(in) :: opts
        character(len=*), intent(in) :: stdoutfile
        character(len=*), intent(in) :: stderrfile
        logical :: chk1
        logical :: chk2

        chk1 = .false.
        chk2 = .false.
        if (opts%rij) then
            call system('ridft > '//stdoutfile//' 2> '//stderrfile)
            chk1 = strinfile(stdoutfile, '****  ridft : all done  ****')
        else
            call system('dscf > '//stdoutfile//' 2> '//stderrfile)
            chk1 = strinfile(stdoutfile, '****  dscf : all done  ****')
            chk2 = strinfile(stdoutfile, 'ATTENTION: dscf did not converge!')
        end if

        if (.not. chk1) then
            write(stderr, *)
            write(stderr, *) 'Error in TurbomoleRunMod. TurboRunSCF subroutine.'
            write(stderr, *) 'SCF calculation failed.'
            stop
        end if
        if (chk2) then
            write(stderr, *)
            write(stderr, *) 'Error in TurbomoleRunMod. TurboRunSCF subroutine.'
            write(stderr, *) 'SCF calculation did not converge.'
            stop
        end if
        
    end subroutine turborunscf


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboRunEx
    !----------------------------------------------------------------------------------------------
    subroutine turborunex(opts, cstate, stdoutfile, stderrfile)
        ! Import classes
        use tmoptstype
        type(tmopts), intent(in) :: opts
        integer, intent(in) :: cstate
        character(len=*), intent(in) :: stdoutfile
        character(len=*), intent(in) :: stderrfile
        logical :: chk1
        logical :: chk2

        chk1 = .false.
        chk2 = .false.
        if (opts%dft) then
            if (cstate > 0) then
                call system('egrad >> '//stdoutfile//' 2>> '//stderrfile)
                chk1 = .true.
                chk2 = strinfile(stdoutfile, '****  egrad : all done  ****')
            else
                if (opts%rij) then
                    call system('rdgrad >> '//stdoutfile//' 2>> '//stderrfile)
                    chk1 = strinfile(stdoutfile, '****  rdgrad : all done  ****')
                else
                    call system('grad >> '//stdoutfile//' 2>> '//stderrfile)
                    chk1 = strinfile(stdoutfile, '****  grad : all done  ****')
                end if
                if (opts%nexstate > 0) then
                    call system('escf >> '//stdoutfile//' 2>> '//stderrfile)
                    chk2 = strinfile(stdoutfile, '****  escf : all done  ****')
                else
                    chk2 = .true.
                end if
            end if
        else if (opts%ricc2) then
            call system('ricc2 >> '//stdoutfile//' 2>> '//stderrfile)
            chk1 = .true.
            chk2 = strinfile(stdoutfile, '****  ricc2 : all done  ****')
        end if

        if (.not. chk1) then
            write(stderr, *)
            write(stderr, *) 'Error in TurbomoleRunMod. TurboRunEx subroutine.'
            write(stderr, *) 'Gradient calculation failed.'
            stop
        end if
        if (.not. chk2) then
            write(stderr, *)
            write(stderr, *) 'Error in TurbomoleRunMod. TurboRunEx subroutine.'
            write(stderr, *) 'Excited state calculation failed.'
            stop
        end if

    end subroutine turborunex



end module turbomolerunmod
