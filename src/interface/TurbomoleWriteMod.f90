!--------------------------------------------------------------------------------------------------
! MODULE: TurbomoleWriteMod
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date November, 2016
!
! DESCRIPTION: 
!> @brief Subroutines for writing turbomole files.
!> @details
!! Write files in a format that can be read by Turbomole modules. Each subroutine writes a single
!! file type.
!--------------------------------------------------------------------------------------------------
module TurbomoleWriteMod
    ! Import variables
    use fortutils
    implicit none

    private
    public :: turbowritecoord
    public :: turbowritebasis
    public :: turbowritemos
    public :: turbowritepcxyz
    public :: turbowritecontrolgrad

contains

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboWriteCoord
    !
    ! DESCRIPTION:
    !> @brief Create a Turbomole coord file.
    !
    !> @todo A new file is always created, if a file with the same name exists it is overwritten.
    !! This can be changed to only update the correct section of a file if it already exists.
    !----------------------------------------------------------------------------------------------
    subroutine turbowritecoord(fname, geom, asym)
        character(len=*), intent(in) :: fname
        real(dp), intent(in) :: geom(:, :)
        character(len=2), intent(in) :: asym(:)
        integer :: outunit
        integer :: i
       
        open(newunit=outunit, file=fname, action='write')
        write(outunit,'(a)') '$coord'
        do i = 1, size(asym)
            write(outunit, '(3(f21.15,2x),4x,a)') geom(:, i), trim(asym(i))
        end do
        write(outunit, '(a)') '$end'
        close(outunit)
    end subroutine turbowritecoord


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboWriteBasis
    !
    ! DESCRIPTION:
    !> @brief Create a Turbomole basis file.
    !
    !> @todo A new file is always created, if a file with the same name exists it is overwritten.
    !! This can be changed to only update the correct section of a file if it already exists.
    !----------------------------------------------------------------------------------------------
    subroutine turbowritebasis(fname, abas)
        ! Import classes
        use atombasistype
        character(len=*), intent(in) :: fname !< Name of output file.
        type(atombasis), allocatable, intent(in) :: abas(:) !< Atomic basis sets.
        integer :: outunit
        integer :: i
        integer :: j
        integer :: k
       
        if (.not. allocated(abas)) then
            write(stderr, *) 'Error in TurbomoleWrite module, TurboWriteBasis subroutine.'
            write(stderr, *) '  Atomic basis variable not allocated.'
            stop
        end if
       
        open(newunit=outunit, file=fname, action='write')
        write(outunit, '(a)') '$basis'
        write(outunit, '(a)') '*'
outer:  do i = 1, size(abas)
            do j = 1, i-1
                if (abas(j)%key == abas(i)%key) cycle outer
            end do
            write(outunit, '(a)') abas(i)%key
            write(outunit, '(a)') '*'
            do j = 1, abas(i)%nfunc
                write(outunit,'(3x,i0,2x,a)') abas(i)%npr(j), abas(i)%typ(j)
                do k = 1, abas(i)%npr(j)
                    write(outunit,*) abas(i)%z(j)%c(k), abas(i)%b(j)%c(k)
                end do
            end do
            write(outunit, '(a)') '*'
        end do outer
        write(outunit, '(a)') '$end'
        close(outunit)
    end subroutine turbowritebasis


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboWriteMos
    !
    ! DESCRIPTION:
    !> @brief Create a Turbomole mos file.
    !> @param fnamea Name of output file for alpha orbitals.
    !> @param fnameb Name of output file for beta orbitals.
    !> @param mo Molecular orbitals of the system.
    !> @param newfmt Non-default format for the write statement.
    !
    !> @todo A new file is always created, if a file with the same name exists it is overwritten.
    !! This can be changed to only update the correct section of a file if it already exists.
    !----------------------------------------------------------------------------------------------
    subroutine turbowritemos(fnamea, fnameb, rhf, nmo, nbf, moc, moe, newfmt)
        ! Import classes
        use stringmod
        character(len=*), intent(in) :: fnamea
        character(len=*), intent(in) :: fnameb
        integer, intent(in) :: rhf
        integer, intent(in) :: nmo
        integer, intent(in) :: nbf
        real(dp), intent(in) :: moc(nbf, nmo, rhf)
        real(dp), intent(in) :: moe(nmo, rhf)
        character(len=*), intent(in), optional :: newfmt

        integer :: outunit
        integer :: i
        integer :: j
        integer :: k
        character(len=:), allocatable :: fname
        character(len=:), allocatable :: writeformat
        character(len=:), allocatable :: key
        character(len=6), parameter :: key1 = '$scfmo'
        character(len=12), parameter :: key2a = '$uhfmo_alpha'
        character(len=11), parameter :: key2b = '$uhfmo_beta'
        character(len=80) :: writeline
        integer :: ninline

        writeformat = '(4d20.14)'
        if (present(newfmt)) writeformat=trim(adjustl(newfmt))
        line%str = writeformat
        call line%parse('(def)')
        read(line%args(1), *) ninline


        do i = 1, rhf
            if (i == 1) then
                if (rhf == 1) key = key1
                if (rhf == 2) key = key2a
                fname = trim(adjustl(fnamea))
            else if (i == 2) then
                key = key2b
                fname = trim(adjustl(fnameb))
            end if
            open(newunit=outunit, file=fname, action='write')
            write(writeline, 1001) key, writeformat
            write(outunit, '(a)') writeline
            do j = 1, nmo
                write(writeline, 1002) j, moe(j, i), nbf
                write(outunit, '(a)') writeline
                do k = 1, nbf, ninline
                    write(outunit, writeformat) moc(k:min(nbf, k + ninline - 1), j, i)
                end do
            end do 
            write(outunit,'(a)') '$end'
            close(outunit)
        end do

    1001 format (a,4x,'scfconv=0',3x,'format',a)
    1002 format (i6,2x,'a  ',4x,'eigenvalue=',d20.14,3x,'nsaos=',i0)
        
    end subroutine turbowritemos
 

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboWritePCxyz
    !
    ! DESCRIPTION:
    !> @brief Update the coordinates in a Turbomole point_charges section.
    !> @todo add error checks.
    !----------------------------------------------------------------------------------------------
    subroutine turbowritepcxyz(fnames, pc_geo)
        use stringmod
        use filestype
        type(files), intent(in) :: fnames
        real(dp), intent(in) :: pc_geo(:, :)
        integer :: unit1
        integer :: unit2
        integer :: i
        character(len=:), allocatable :: q
        character(len=17), parameter:: tempfile='temp_pc_file'

        open(newunit=unit1, file=fnames%fn('pointcharges'), action='read')
        open(newunit=unit2, file=tempfile, action='write')
        do
            call line%readline(unit1)
            if (index(line%str, '$end') /= 0) exit
            if (index(line%str, '$point_charges') == 0) cycle
            write(unit2, '(a)') line%str
            do i = 1, size(pc_geo, 2)
                call line%readline(unit1)
                call line%parse(' ,')
                q = trim(line%args(line%narg))
                write(unit2, '(3f20.15,2x,a)') pc_geo(:, i), q
            end do
        end do
        write(unit2, '(a)') '$end'
        close(unit=unit1)
        close(unit=unit2)

        call system('mv '//tempfile//' '//fnames%fn('pointcharges'))

    end subroutine turbowritepcxyz



    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboWriteControlGrad
    !
    ! DESCRIPTION:
    !> @brief Update the control file to request gradient of the specified state.
    !----------------------------------------------------------------------------------------------
    subroutine turbowritecontrolgrad(fnames, opts, cstate)
        use stringmod
        use filestype
        use tmoptstype
        type(files), intent(in) :: fnames
        type(tmopts), intent(inout) :: opts
        integer, intent(in) :: cstate
        integer :: unit1
        integer :: unit2
        logical :: updated
        character(len=17), parameter:: tempfile='temp_control_file'
        character(len=:), allocatable :: model
        character(len=:), allocatable :: state
        character(len=20) :: tempstr

        if (opts%ricc2) then
            select case (opts%ricc2model)
            case(2)
                model = 'mp2'
            case(4)
                model = 'adc(2)'
            case(5)
                model = 'cc2'
            case(6)
                model = 'ccsd'
            end select
        end if

        if (cstate == 0) then
            state = '(x)'
        else
            write(tempstr, '(a,i0,a)') '(a ', cstate, ')'
            state = trim(adjustl(tempstr))
        end if

        updated = .false.
        open(newunit=unit1, file=fnames%fn('control'), action='READ')
        open(newunit=unit2, file=tempfile, action='WRITE')
        do
            call line%readline(unit1)
100         if (index(line%str, '$end')/=0) exit
            ! For tddft calculations look for the $exopt section.
            if (opts%dft) then
                ! Just update the keyword and continue.
                if (index(line%str, '$exopt') /= 0) then
                    write(unit2, '(a, i0)') '$exopt ', cstate
                    updated=.true.
                    cycle
                end if
            ! For ricc2 calculations look for the $excitations and $response sections.
            else if (opts%ricc2) then
                if (index(line%str, '$ricc2') /= 0) then
                    write(unit2, '(a)') line%str
                    do
                        call line%readline(unit1)
                        if (index(line%str, '$') == 1) then
                            if (.not. updated) then
                                write(unit2, '(a)') '  geoopt model='//model//'    state='//state
                                updated = .true.
                            end if
                            goto 100
                        else if (index(line%str, 'geoopt') /= 0) then
                            write(unit2, '(a)') '  geoopt model='//model//'    state='//state
                            updated = .true.
                        else
                            write(unit2, '(a)') line%str
                        end if
                    end do
                end if
            
            end if
            write(unit2, '(a)') line%str
        end do

        if (.not. updated) then
            if (opts%dft) then
                write(unit2, '(a, i0)') '$exopt ', cstate
                updated = .true.
            end if
        end if
        if (.not. updated) then
            write(stderr,*) 'Error in TurbomoleWrite module, TurboWriteControlGrad subroutine.'
            write(stderr,'(2x,a)') 'Failed to update control file.'
            stop
        end if

        write(unit2, '(a)') '$end'
        close(unit=unit1)
        close(unit=unit2)
        call system('mv '//tempfile//' '//fnames%fn('control'))

    end subroutine turbowritecontrolgrad



end module
