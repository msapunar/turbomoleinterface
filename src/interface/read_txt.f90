module read_txt_mod
    use fortutils
    implicit none


    interface read_txt
        module procedure read_txt_dim2
        module procedure read_txt_dim3
    end interface read_txt

contains


    subroutine read_txt_dim2(fname, n1, n2, array)
        character(len=*), intent(in) :: fname
        integer, intent(inout) :: n1
        integer, intent(inout) :: n2
        real(dp), allocatable, intent(out) :: array(:, :)
        integer :: d1, d2, i, iunit

        open(newunit=iunit, file=fname)
        read(iunit, *) d1, d2
        if ((n1 /= 0) .and. (n1 /= d1)) call dim_error(fname, 1, n1, d1)
        if ((n2 /= 0) .and. (n2 /= d2)) call dim_error(fname, 2, n2, d2)
        n1 = d1
        n2 = d2
        if (allocated(array)) deallocate(array)
        allocate(array(n1, n2))
        do i = 1, n2
            read(iunit, *) array(:, i)
        end do
        close(iunit)
    end subroutine read_txt_dim2


    subroutine read_txt_dim3(fname, n1, n2, n3, array)
        character(len=*), intent(in) :: fname
        integer, intent(inout) :: n1
        integer, intent(inout) :: n2
        integer, intent(inout) :: n3
        real(dp), allocatable, intent(out) :: array(:, :, :)
        integer :: d1, d2, d3, i, j, iunit

        open(newunit=iunit, file=fname)
        read(iunit, *) d1, d2, d3
        if ((n1 /= 0) .and. (n1 /= d1)) call dim_error(fname, 1, n1, d1)
        if ((n2 /= 0) .and. (n2 /= d2)) call dim_error(fname, 2, n2, d2)
        if ((n3 /= 0) .and. (n3 /= d3)) call dim_error(fname, 3, n3, d3)
        n1 = d1
        n2 = d2
        n3 = d3
        if (allocated(array)) deallocate(array)
        allocate(array(n1, n2, n3))
        do i = 1, n3
            do j = 1, n2
                read(iunit, *) array(:, j, i)
            end do
        end do
        close(iunit)
    end subroutine read_txt_dim3


    subroutine dim_error(fname, cdim, n, d)
        character(len=*), intent(in) :: fname
        integer, intent(in) :: cdim
        integer, intent(in) :: n
        integer, intent(in) :: d

        write(stderr, '(a)') 'Error reading array from file ', trim(fname), '.'
        write(stderr, '(1x, a, i0)') 'For array dimension: ', cdim
        write(stderr, '(3x, a, i0, a, i0, a)') 'Expected size ', n, ' instead of ', d, '.'
    end subroutine dim_error


end module read_txt_mod
