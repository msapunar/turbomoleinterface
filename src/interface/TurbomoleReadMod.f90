!----------------------------------------------------------------------------------------------
! MODULE: TurbomoleReadMod
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date November, 2016
!
! DESCRIPTION:
!> @brief Contains subroutines for reading Turbomole input and output files.
!> @details
!! Most of the Turbomole output input is organized into files with sections marked by keywords
!! (ex. $basis). Each subroutine in this module reads a section from various Turbomole files.
!! Mostly, it should be possible to call the subroutines in any order, as the subroutines will 
!! attempt to read any missing required information by calling each other.
!! All subroutines make use of the files type to keep track of various sections to be read.
!! Options about the calculation (read from the control file) are kept in a tmopts variable.
!----------------------------------------------------------------------------------------------
 module turbomolereadmod
    ! Import variables
    use fortutils
    ! Import classes
    use stringmod
    use filestype
    use tmoptstype
    implicit none

    private
    public turboreadall
    ! Control file
    public :: turboreadfns
    public :: turboreadopts
    public :: turboreadrundim, turboreadatoms!, turboreadorbocc, turboreadorbact
    ! Coord file
    public :: turboreadcoord
    ! Basis file
    public :: turboreadbasis
    ! Molecular orbitals file(s)
    public :: turboreadmos
    ! Wave functions.
    public :: turboreadwf
    ! Energies
    public :: turboreaden
    ! Oscillator strengths
    public :: turboreados
    ! Gradients
    public :: turboreadgrad
    ! Hessian
    public :: turboreadhess

contains


    
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboReadAll
    !----------------------------------------------------------------------------------------------
    subroutine turboreadall(fnames, opts, qm, awfopt)
        use ccgmod
        use qminfotype
        type(files), intent(inout) :: fnames !< List of Turbomole file names.
        type(tmopts), intent(inout) :: opts !< Options in the Turbomole control file.
        type(qminfo), intent(inout) :: qm !< Information about the system.
        integer, optional, intent(in) :: awfopt !< Option for reading TDDFT aux wf.
        integer :: topt
        logical :: check

        topt = 1
        call turboreadopts(fnames, opts)
        call turboreadcoord(fnames, qm%nat, qm%axyz, qm%asym)
        call turboreadbasis(fnames, qm%abas)
        call genccgs(qm%axyz, qm%abas, qm%naos, qm%naoc, qm%ao_c, qm%ao_c2s)
        call turboreadmos(fnames, qm%rhf, qm%nmo, qm%naos, qm%mo_c, qm%mo_en)
        call turboreadorbocc(fnames, qm%rhf, qm%nmo, qm%mo_omask, qm%mo_o)
        call turboreadorbact(fnames, qm%rhf, qm%nmo, qm%mo_amask)
        call qm%countnmo()
        call turboreaden(fnames, opts, qm%en)
        qm%nexst = opts%nexstate
        if (opts%nexstate > 0) then
            if (opts%spectrum .or. opts%ricc2spectrum) call turboreados(fnames, opts, qm%exos)
            if (present(awfopt)) topt = awfopt
            call turboreadwf(fnames, opts, topt, qm%rhf, qm%nmo_ao, qm%nmo_av, qm%nexst, qm%ndet,&
                           & qm%exen, qm%wf(1)%c, qm%wf(2)%c)
            qm%nst = qm%nexst + 1
        end if
        if (opts%gradient >= 0) then
            inquire(file=fnames%fn('gradient'), exist=check)
            if (check) then
                qm%cst = opts%gradient
                call turboreadgrad(fnames, qm%nat, qm%agrd)
            end if
        end if
    end subroutine turboreadall


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboReadFNs
    !
    ! DESCRIPTION:
    !> @brief Read names of various Turbomole files from a Turbomole control file.
    !> @details
    !! The Turbomole control file contains names of sections followed by 'file=...'. If a filename
    !! is not given after the keyword, the section is in the control file.
    !----------------------------------------------------------------------------------------------
    subroutine turboreadfns(fnames)
        type(files), intent(inout) :: fnames !< List of Turbomole file names.
        integer :: inunit
        logical :: check

     
        if (fnames%q('control', 0) == 0) call fnames%add('control', 'control')
        inquire(file=fnames%fn('control'), exist=check)
        if (.not. check) then
            write(stderr,*) 'Error in TurbomoleRead module, TurboReadFNs subroutine.'
            write(stderr,*) ' Control file (', fnames%fn('control'),') not found.'
            stop
        end if
     
        open(newunit=inunit, file=fnames%fn('control'), action='read')
        do
            call line%readline(inunit, comment='#')
            call line%parse(' ')
            if (line%args(1) == '$coord') then
                call fnames%readfn(line, 'coord', 0, 'control')
            else if (line%args(1) == '$atoms') then
                call fnames%readfn(line, 'atoms', 0, 'control')
            else if (line%args(1) == '$basis') then
                call fnames%readfn(line, 'basis', 0, 'control')
            else if (line%args(1) == '$energy') then
                call fnames%readfn(line, 'energy', 0, 'control')
            else if (line%args(1) == '$grad') then
                call fnames%readfn(line, 'gradient', 0, 'control')
            else if (line%args(1) == '$hessian') then
                call fnames%readfn(line, 'hessian', 0, 'control')
            else if (line%args(1) == '$point_charges') then
                call fnames%readfn(line, 'pointcharges', 0, 'control')
            else if (line%args(1) == '$rundimensions') then
                call fnames%readfn(line, 'rundimensions', 0, 'control')
            else if (line%args(1) == '$scfmo') then
                call fnames%readfn(line, 'mos', 1, 'control')
            else if (line%args(1) == '$uhfmo_alpha') then
                call fnames%readfn(line, 'mos', 1, 'control')
            else if (line%args(1) == '$uhfmo_beta') then
                call fnames%readfn(line, 'mos', 2, 'control')
            else if (index(line%args(1), '$excitation_energies') /= 0) then
                call fnames%readfn(line, trim(adjustl(line%args(1))), 0, 'control')
            else if (index(line%args(1), '$exstprop') /= 0) then
                call fnames%readfn(line, trim(adjustl(line%args(1))), 0, 'control')
            else if (index(line%args(1), '$tranprop') /= 0) then
                call fnames%readfn(line, trim(adjustl(line%args(1))), 0, 'control')
            else if (line%args(1) == '$end') then
                exit
            end if
        end do
        close(inunit)

        if (fnames%q('mos', 2) == 0) call fnames%add('mos', '', 2)
    end subroutine turboreadfns


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboReadOptions
    !
    ! DESCRIPTION:
    !> @brief Read Determine type of calculation from TM control file.
    !> @details
    !! v0.01
    !
    !> @param opts Type containing the options from a turbomole control file.
    !----------------------------------------------------------------------------------------------
    subroutine turboreadopts(fnames, opts)
        type(files), intent(inout) :: fnames !< List of file names.
        type(tmopts), intent(inout) :: opts
        integer :: inunit
        integer :: i

        opts%hf = .true.
         
        if (fnames%q('control', 0) == 0) call turboreadfns(fnames)
        open(newunit=inunit, file=fnames%fn('control'), action='read')
outer:  do
            call line%readline(inunit, comment='#')
1001        if (index(line%str, '$end') == 1) exit outer

            ! Determine symmetry: 
            if (index(line%str, '$symmetry') == 1) then
                    call line%parse(' ')
                    opts%symmetry = trim(adjustl(line%args(2)))
                    if (opts%symmetry /= 'c1') then
                        write(stderr, *)
                        write(stderr, *) 'Warning in TurbomoleRead module, TurboReadOpts subroutine.'
                        write(stderr, *) '  Most subroutines assume C1 symmetry.'
                    end if

            ! Spectrum
            else if (index(line%str, '$spectrum') == 1) then
                opts%spectrum = .true.
                call fnames%add('spectrum', 'spectrum')
            

            ! DFT options:
            ! DFT flag:
            else if (index(line%str, '$dft') == 1) then
                opts%dft = .true.
                do
                    call line%readline(inunit, comment='#')
                    if (index(line%str, '$') == 1) goto 1001
                    call line%parse(' ')
                    if (line%args(1) == 'functional') then
                        opts%func = trim(adjustl(line%args(2)))
                        opts%functype = functionaltype(opts%func)
                    end if
                end do
            ! Excited states calculation type:
            else if (index(line%str, '$scfinstab') == 1) then
                call line%parse(' ')
                select case (line%args(2))
                case ('rpas')
                    opts%instab = 1
                    call fnames%add('eigenpairs', 'sing_a')
                case ('ciss')
                    opts%instab = 2
                    call fnames%add('eigenpairs', 'ciss_a')
                case ('urpa')
                    opts%instab = 1
                    call fnames%add('eigenpairs', 'unrs_a')
                case ('ucis')
                    opts%instab = 2
                    call fnames%add('eigenpairs', 'ucis_a')
                end select
            ! Number of excited states:
            else if (index(line%str, '$soes') == 1) then
                call line%readline(inunit, comment='#')
                call line%parse(' ')
                read(line%args(2), *) opts%nexstate
            ! Excited state gradient calculation:
            else if (index(line%str, '$exopt') == 1) then
                call line%parse(' ')
                read(line%args(2), *) opts%gradient

            ! RICC2 options:
            ! RICC2 flag:
            else if (index(line%str, '$ricc2') == 1) then
                opts%ricc2 = .true.
                call fnames%add('restartcc', 'restart.cc')
                do
                    call line%readline(inunit, comment='#')
                    if (index(line%str, '$') == 1) goto 1001
                    call line%parse(' =')
                    select case (line%args(1))
                    case('mp2')
                        opts%ricc2model = 2
                    case('adc(2)')
                        opts%ricc2model = 4
                    case('cc2')
                        opts%ricc2model = 5
                    case('ccsd')
                        opts%ricc2model = 6
                    case('geoopt')
                        call line%parse(' =()')
                        do i = 2, line%narg - 1
                            if (line%args(i) == 'state') then
                                if (line%args(i+1) == 'x') then
                                    opts%gradient = 0
                                else
                                    read(line%args(i+2), *) opts%gradient
                                end if
                                exit
                            end if
                        end do
                    end select
                end do
            ! Number of excited states:
            else if (index(line%str, '$excitations') == 1) then
                do
                    call line%readline(inunit, comment='#')
                    if (index(line%str, '$') == 1) goto 1001
                    if (index(line%str, 'irrep') == 1) then
                        call line%parse(' =')
                        do i = 1, line%narg - 1
                            if (line%args(i) == 'nexc') then
                                read(line%args(i+1), *) opts%nexstate
                            end if
                        end do
                    else if (index(line%str, 'spectrum') == 1) then
                        opts%ricc2spectrum = .true.
                    end if
                end do

            ! Point charges    
            else if (index(line%str, '$point_charges') == 1) then
                opts%pcharges = .true.

            ! RI flag:
            else if (index(line%str, '$rij') == 1) then
                opts%rij = .true.
            end if
        end do outer
        close(inunit)

        if (opts%dft .or. opts%ricc2) then
            opts%hf = .false.
        end if
        if (opts%ricc2) then
            if (opts%ricc2model == 6) then
                opts%spectrum = .false.
                opts%ricc2spectrum = .false.
            end if
        end if
        if (opts%dft .or. opts%hf) then
            if (opts%gradient < 0) opts%gradient = 0
        end if

        opts%inited = .true.

    end subroutine turboreadopts
 
    
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboReadRundim
    !
    ! DESCRIPTION:
    !> @brief Read basic information from the $rundimensions section of a TM control file.
    !> @details
    !! All parameters are optional and should be set to zero if they are being read for the first
    !! time in this subroutine. If a nonzero parameter is passed, the subroutine checks if the
    !! value is consistent with the control file.
    !----------------------------------------------------------------------------------------------
    subroutine turboreadrundim(fnames, rhf, natom, nsphe, ncart)
        type(files), intent(inout) :: fnames !< List of file names.
        integer, intent(inout), optional :: rhf !< Restriced (1) or unrestricted (2) calculation.
        integer, intent(inout), optional :: natom !< Number of atoms.
        integer, intent(inout), optional :: nsphe !< Number of spherical basis functions.
        integer, intent(inout), optional :: ncart !< Number of cartesian basis functions.
        integer :: trhf
        integer :: tnatom
        integer :: tnsphe
        integer :: tncart
        integer :: inunit

        if (fnames%q('rundimensions', 0) == 0) call turboreadfns(fnames)
        open(newunit=inunit, file=fnames%fn('rundimensions'), action='read')
 outer: do
            call line%readline(inunit, comment='#')
            if (index(line%str, '$rundimensions') == 1) then
 inner:         do
                    call line%readline(inunit, comment='#')
                    if (index(line%str, '$') /= 0) exit outer
                    call line%parse('=')
                    select case (line%args(1))
                        case('natoms')
                            read(line%args(2),*) tnatom
                        case('nbf(CAO)')
                            read(line%args(2),*) tncart
                        case('nbf(AO)')
                            read(line%args(2),*) tnsphe
                        case('rhfshells')
                            read(line%args(2),*) trhf
                    end select
                    cycle
                end do inner
            end if
        end do outer
        close(inunit)

        ! Consistency check
        if (present(natom)) then
            if (natom == 0) then
                natom = tnatom
            else if (natom /= tnatom) then
                write(stderr, *)
                write(stderr, *) 'Error in TurbomoleRead module, TurboReadRundim subroutine.'
                write(stderr, *) '  Number of atoms mismatch.'
                stop
            end if
        end if
        if (present(ncart)) then
            if (ncart == 0) then
                ncart = tncart
            else if (ncart /= tncart) then
                write(stderr, *) 
                write(stderr, *) 'Error in TurbomoleRead module, TurboReadRundim subroutine.'
                write(stderr, *) '  Number of cartesian basis functions mismatch.'
                stop
            end if
        end if
        if (present(nsphe)) then
            if (nsphe == 0) then
                nsphe = tnsphe
            else if (nsphe /= nsphe) then
                write(stderr, *) 
                write(stderr, *) 'Error in TurbomoleRead module, TurboReadRundim subroutine.'
                write(stderr, *) '  Number of spherical basis functions mismatch.'
                stop
            end if
        end if
        if (present(rhf)) then
            if (rhf == 0) then
                rhf = trhf
            else if (rhf /= trhf) then
                write(stderr, *) 
                write(stderr, *) 'Error in TurbomoleRead module, TurboReadRundim subroutine.'
                write(stderr, *) '  Restricted/unrestricted mismatch.'
                stop
            end if
        end if
    end subroutine turboreadrundim


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboReadAtoms
    !
    ! DESCRIPTION:
    !> @brief Read the $atoms section of the Turbomole control file.
    !> @details
    !! Reads the list of atoms and the name of the basis set for each atom. The name of the basis
    !! set is read into the abas(:)%key variable. This key can be used to locate the basis set in
    !! the turbomole $basis section.
    !----------------------------------------------------------------------------------------------
    subroutine turboreadatoms(fnames, asym, abas)
        use atombasistype
        type(files), intent(inout) :: fnames !< List of file names.
        character(len=2), allocatable :: asym(:) !< List of atom symbols.
        type(atombasis), allocatable :: abas(:) !< Basis sets for each atom.
        
        integer :: inunit
        integer :: i
        integer :: ci
        integer :: natom
        character(len=2) :: catom
        character(len=1000) :: catomindex
        character(len=80) :: cbsetname
        integer, allocatable :: indexlist(:)
        
        if (fnames%q('atoms', 0) == 0) call turboreadfns(fnames)
        if (.not. (allocated(asym) .and. allocated(abas))) then
            natom = 0
            call turboreadrundim(fnames, natom=natom)
            if (.not. allocated(asym)) then
                allocate(asym(natom))
                asym = ''
            end if
            if (.not. allocated(abas)) allocate(abas(natom))
        end if
        natom = size(asym)
        
        open(newunit=inunit, file=fnames%fn('atoms'), action='read')
        do
            call line%readline(inunit, comment='#')
            if (index(line%str, '$atoms') == 1) then
                do
                   call line%readline(inunit, comment='#')
                   if (index(line%str, '$') == 1) exit
                   call line%parse(' ')
                   read(line%args(1), '(a)') catom
                   read(line%args(2), '(a)') catomindex
                   do
                      call line%readline(inunit, comment='#')
                      call line%parse('=\')
                      if (line%args(1) == 'basis') cbsetname = line%args(2)
                      if (index(line%str, '\') == 0) exit
                   end do
                   call readindexlist(catomindex, indexlist)
                   do i = 1, size(indexlist)
                      ci = indexlist(i)
                      if (asym(ci) == '') then
                          asym(ci) = tolower(catom)
                      else if (asym(ci) /= tolower(catom)) then
                          write(stderr, *) 
                          write(stderr, *) 'Error in TurbomoleRead module, &
                                           &TurboReadAtoms subroutine.'
                          write(stderr, *) '  Atom list mismatch.'
                          stop
                      end if
                      abas(ci)%key = trim(adjustl(cbsetname))
                   end do
                end do
                exit
            else if (index(line%str, '$end') == 1) then
                write(stderr, *) 
                write(stderr, *) 'Error in TurbomoleRead module, TurboReadAtoms subroutine.'
                write(stderr, *) '  Section $atoms not found in control file.'
                stop
            end if
        end do
        close(inunit)
    end subroutine turboreadatoms


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboReadOrbOcc
    !
    ! DESCRIPTION:
    !> @brief Read the occupation numbers of molecular orbitals from a Turbomole 'control' file.
    !> @details
    !! The occupation numbers are written in the "$closed shells" section of the control file for 
    !! restricted calculations, and "$alpha shells" and "$beta shells" sections for unrestricted
    !! calculations.
    !----------------------------------------------------------------------------------------------
    subroutine turboreadorbocc(fnames, rhf, nmo, omask, occ)
        type(files), intent(inout) :: fnames !< List of file names.
        integer, intent(inout) :: rhf !< Restricted/unrestricted calculation.
        integer, intent(inout) :: nmo !< Number of molecular orbitals.
        logical, allocatable, intent(inout) :: omask(:, :) !< Occupied orbitals mask.
        real(dp), allocatable, optional, intent(inout) :: occ(:, :) !< Occupation numbers.

        integer :: inunit
        integer :: i
        integer, allocatable :: indexlist(:)
        logical :: foundone
        
        foundone = .false.
        if (fnames%q('control', 0) == 0) call turboreadfns(fnames)
        if (rhf == 0) call turboreadrundim(fnames, rhf=rhf)
        if (nmo == 0) call turboreadrundim(fnames, nsphe=nmo)
        if (.not. allocated(omask)) allocate(omask(nmo, 2))
        omask = .false.
        if (present(occ)) then
            if (.not. allocated(occ)) allocate(occ(nmo, 2))
            occ = 0.0_dp
        end if

        open(newunit=inunit, file=fnames%fn('control'), action='read')    
        do
            call line%readline(inunit, comment='#')
            if (index(line%str, '$end') == 1) then
                write(stderr, *) 
                write(stderr, *) 'Error in TurbomoleRead module, TurboOrbOcc subroutine.'
                if (rhf == 1) write(stderr, *) '  Section $closed shells not found.'
                if (rhf == 2) write(stderr, *) '  Section $alpha(beta) shells not found.'
                stop
            else if (rhf == 1) then
                if (index(line%str, '$closed shells') == 1) then
                    call line%readline(inunit, comment='#')
                    line%str = line%str(index(line%str, ' '):len(line%str))
                    call line%parse('()')
                    call readindexlist(line%args(1), indexlist)
                    do i = 1, size(indexlist)
                        omask(indexlist(i), :) = .true.
                        if (present(occ)) occ(indexlist(i), :) = 2.0_dp
                    end do
                    exit
                end if
            else if (rhf == 2) then
                if (index(line%str, '$alpha shells') == 1) then
                    call line%readline(inunit, comment='#')
                    line%str = line%str(index(line%str, ' '):len(line%str))
                    call line%parse('()')
                    call readindexlist(line%args(1), indexlist)
                    do i = 1, size(indexlist)
                        omask(indexlist(i), 1) = .true.
                        if (present(occ)) occ(indexlist(i), 1) = 1.0_dp
                    end do
                    if (foundone) exit
                    foundone = .true.
                else if (index(line%str, '$beta shells') == 1) then
                    call line%readline(inunit, comment='#')
                    line%str = line%str(index(line%str, ' '):len(line%str))
                    call line%parse('()')
                    call readindexlist(line%args(1), indexlist)
                    do i = 1, size(indexlist)
                        omask(indexlist(i), 2) = .true.
                        if (present(occ)) occ(indexlist(i), 2) = 1.0_dp
                    end do
                    if (foundone) exit
                    foundone = .true.
                end if
            end if
        end do
        close(inunit)
    end subroutine turboreadorbocc


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboReadOrbAct
    !
    ! DESCRIPTION:
    !> @brief Read the occupation numbers of molecular orbitals from a Turbomole 'control' file.
    !> @details
    !! For ricc2 calculations. Information about the inactive orbitals is found in the $freeze
    !! keyword. If the keyword is not present, all orbitals are considered active.
    !----------------------------------------------------------------------------------------------
    subroutine turboreadorbact(fnames, rhf, nmo, amask)
        use motype
        type(files), intent(inout) :: fnames !< List of file names.
        integer, intent(inout) :: rhf !< Restricted/unrestricted calculation.
        integer, intent(inout) :: nmo !< Number of molecular orbitals.
        logical, allocatable :: amask(:) !< Active orbitals mask.

        integer :: inunit
        integer :: i
        integer :: num
        integer, allocatable :: indexlist(:)

        if (fnames%q('control', 0) == 0) call turboreadfns(fnames)
        if (rhf == 0) call turboreadrundim(fnames, rhf=rhf)
        if (nmo == 0) call turboreadrundim(fnames, nsphe=nmo)
        if (.not. allocated(amask)) allocate(amask(nmo))
        amask = .true.

        open(newunit=inunit, file=fnames%fn('control'), action='read')    
        do
            call line%readline(inunit, comment='#')
            if (index(line%str, '$end') == 1) exit
            if (index(line%str, '$freeze') == 1) then
                call line%readline(inunit, comment='#')
                if (index(line%str, 'implicit') == 1) then
                    call line%parse(' =')
                    do i = 1, line%narg - 1
                        if (trim(line%args(i)) == 'core') then
                            read(line%args(i+1), *) num
                            amask(1:num) = .false.
                        else if (trim(line%args(i)) == 'virt') then
                            read(line%args(i+1), *) num
                            num = nmo - num + 1
                            amask(num:) = .false.
                        end if
                    end do
                else
                    line%str = line%str(index(line%str, ' '):len(line%str))
                    call readindexlist(line%str, indexlist)
                    do i = 1, size(indexlist)
                        amask(indexlist(i)) = .false.
                    end do
                end if
            end if
        end do
        close(inunit)
    end subroutine turboreadorbact



    !----------------------------------------------------------------------------------------------
    ! FUNCTION: FunctionalType
    !
    ! DESCRIPTION:
    !> @brief Return type of DFT functional.
    !> @details
    !! The output is an integer based on the type of functional:
    !!  1 - LDA
    !!  2 - GGA
    !!  3 - MGGA
    !!  4 - Hybrid
    !!  5 - ODFT
    !!  6 - Double-hybrid
    !----------------------------------------------------------------------------------------------
    function functionaltype(func) result (functype)
        character(len=*), intent(in) :: func
        integer :: functype
        character(len=21), dimension(5), parameter :: ldafunc = ['slater-dirac-exchange', &
                                                                 's-vwn                ', &
                                                                 'vwn                  ', &
                                                                 's-vwn_Gaussian       ', &
                                                                 'pwlda                ']
        character(len=14), dimension(7), parameter :: ggafunc = ['becke-exchange', &
                                                                 'b-lyp         ', &
                                                                 'b-vwn         ', &
                                                                 'lyp           ', &
                                                                 'b-p           ', &
                                                                 'pbe           ', &
                                                                 'b97-d         ']
        character(len=4 ), dimension(1), parameter :: mggfunc = ['tpss']
        character(len=15), dimension(8), parameter :: hybfunc = ['bh-lyp         ', &
                                                                 'b3-lyp         ', &
                                                                 'b3-lyp_Gaussian', &
                                                                 'pbe0           ', &
                                                                 'tpshh          ', &
                                                                 'm06            ', &
                                                                 'm06-2x         ', &
                                                                 'pbeh-3c        ']
        character(len=3 ), dimension(2), parameter :: odffunc = ['lhf', &
                                                                 'oep']
        character(len=7 ), dimension(1), parameter :: dhyfunc = ['b2-plyp']
        functype = 0
        if (any(ldafunc == func)) functype = 1
        if (any(ggafunc == func)) functype = 2
        if (any(mggfunc == func)) functype = 3
        if (any(hybfunc == func)) functype = 4
        if (any(odffunc == func)) functype = 5
        if (any(dhyfunc == func)) functype = 6
        if (functype == 0) then
            write(stderr, *) 
            write(stderr, *) 'Error in TurbomoleRead module, FunctionalType subroutine.'
            write(stderr, *) '  Unrecognized DFT functional.'
            stop
        end if
    end function functionaltype


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboReadCoord
    !
    ! DESCRIPTION:
    !> @brief Read the Turbomole $coord section.
    !> @details
    !! Read the coordinates of each atom into the atom%xyz variable. Also initializes the atom 
    !! type if needed.
    !----------------------------------------------------------------------------------------------
    subroutine turboreadcoord(fnames, natom, geom, asym)
        type(files), intent(inout) :: fnames !< List of file names.
        integer, intent(inout) :: natom !< Number of atoms.
        real(dp), allocatable, intent(inout) :: geom(:, :) !< Coordinates of atoms.
        character(len=2), allocatable, intent(out), optional :: asym(:) !< Atom symbols.
        integer :: i
        character(len=2) :: tsym
        integer :: inunit

        if (fnames%q('coord', 0) == 0) call turboreadfns(fnames)
        if (natom == 0) call turboreadrundim(fnames, natom = natom)
        if (.not. allocated(geom)) allocate(geom(3, natom))
        if (present(asym)) then
            if (.not. allocated(asym)) allocate(asym(natom))
        end if

        open(newunit=inunit, file=fnames%fn('coord'), action='read')
        do
            call line%readline(inunit, comment='#')
            if (index(line%str, '$coord') /= 1) cycle
            do i = 1, natom
                call line%readline(inunit, comment='#')
                read(line%str, *) geom(:, i), tsym
                if (present(asym)) asym(i) = tsym
            end do
            exit
        end do
        close(inunit)
    end subroutine turboreadcoord
 

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboReadBasis
    !
    ! DESCRIPTION:
    !> @brief Read the Turbomole $basis section.
    !> @details
    !! Read the full basis set of each atom into the atom%bas variable. 
    !----------------------------------------------------------------------------------------------
    subroutine turboreadbasis(fnames, abas)
        use atombasistype
        type(files), intent(inout) :: fnames !< List of file names.
        type(atombasis), allocatable, intent(inout) :: abas(:) !< Basis set for each atom.
       
        character(len=2), allocatable :: asym(:)
        integer :: inunit
        integer :: i
        integer :: j
        integer :: k
        character, parameter :: orborder(6) = ['s', 'p', 'd', 'f', 'g', 'h']
        integer, parameter :: maxnfunc = 100
        integer, parameter :: maxnprim = 25
        integer :: nfunc
        integer :: ncart
        integer :: nsphe
        integer :: nprim(maxnfunc)
        real(dp) :: zeta(maxnfunc, maxnprim)
        real(dp) :: beta(maxnfunc, maxnprim)
        character :: orbtype(maxnfunc)
        integer :: cfunc
       
        if (fnames%q('basis', 0) == 0) call turboreadfns(fnames)
        if (.not. allocated(abas)) call turboreadatoms(fnames, asym, abas)
        do i = 1, size(abas)
            if (.not. allocated(abas(i)%key)) call turboreadatoms(fnames, asym, abas)
        end do
       
        open(newunit=inunit, file=fnames%fn('basis'), action='read')
        do i = 1, size(abas)
            nfunc = 0
            ncart = 0
            nsphe = 0
            do
                call line%readline(inunit, comment='#')
                if (index(line%str, '$basis') == 1) then
                    do
                        call line%readline(inunit, comment='#')
                        if (line%str == trim(adjustl(abas(i)%key))) then
                            call line%readline(inunit, comment='#')
                            if (index(line%str, '*') /= 1) then
                                write(stderr, *) 
                                write(stderr, *) 'Error in TurbomoleRead module, &
                                                 &TurboReadBasis subroutine.'
                                write(stderr, *) '  Unexpected format in basis file.'
                                stop
                            end if
                            do
                                call line%readline(inunit, comment='#')
                                if (index(line%str, '*') == 1) exit
                                nfunc = nfunc + 1
                                read(line%str, *) nprim(nfunc), orbtype(nfunc)
                                do j = 1, nprim(nfunc)
                                    call line%readline(inunit, comment='#')
                                    read(line%str, *) zeta(nfunc,j), beta(nfunc,j)
                                end do
                            end do
                            exit
                        else if (index(line%str, '$') == 1) then
                            write(stderr, *) 
                            write(stderr, *) 'Error in TurbomoleRead module, &
                                             &TurboReadBasis subroutine.'
                            write(stderr, *) '  Basis set not found in basis file.'
                            stop
                        end if
                    end do
                    exit
                else if (index(line%str, '$end') == 1) then
                    write(stderr, *) 
                    write(stderr, *) 'Error in TurbomoleRead module, TurboReadBasis subroutine.'
                    write(stderr, *) '  Section $basis not found.'
                    stop
                end if
            end do
            cfunc = 0
            abas(i)%nfunc = nfunc
            if (.not. allocated(abas(i)%typ)) allocate(abas(i)%typ(nfunc))
            if (.not. allocated(abas(i)%l)) allocate(abas(i)%l(nfunc))
            if (.not. allocated(abas(i)%npr)) allocate(abas(i)%npr(nfunc))
            if (.not. allocated(abas(i)%z)) allocate(abas(i)%z(nfunc))
            if (.not. allocated(abas(i)%b)) allocate(abas(i)%b(nfunc))
            do j = 1, size(orborder)
                do k = 1, nfunc
                    if (orbtype(k) == orborder(j)) then
                        cfunc = cfunc + 1
                        abas(i)%typ(cfunc) = orbtype(k)
                        abas(i)%npr(cfunc) = nprim(k)
                        if (.not. allocated(abas(i)%z(cfunc)%c)) then
                            allocate(abas(i)%z(cfunc)%c(nprim(k)))
                        end if
                        if (.not. allocated(abas(i)%b(cfunc)%c)) then
                            allocate(abas(i)%b(cfunc)%c(nprim(k)))
                        end if
                        abas(i)%z(cfunc)%c = zeta(k, 1:nprim(k))
                        abas(i)%b(cfunc)%c = beta(k, 1:nprim(k))
                        select case (orbtype(k))
                            case('s')
                                abas(i)%l(cfunc) = 0
                                nsphe = nsphe + 1
                                ncart = ncart + 1
                            case('p')
                                abas(i)%l(cfunc) = 1
                                nsphe = nsphe + 3
                                ncart = ncart + 3
                            case('d')
                                abas(i)%l(cfunc) = 2
                                nsphe = nsphe + 5
                                ncart = ncart + 6
                            case('f')
                                abas(i)%l(cfunc) = 3
                                nsphe = nsphe + 7
                                ncart = ncart + 10
                            case('g')
                                abas(i)%l(cfunc) = 4
                                nsphe = nsphe + 9
                                ncart = ncart + 15
                            case('h')
                                abas(i)%l(cfunc) = 5
                                nsphe = nsphe + 11
                                ncart = ncart + 21
                        end select
                    end if
                end do
                if (cfunc == nfunc) exit
                if (j == size(orborder)) then
                    write(stderr, *) 
                    write(stderr, *) 'Error in TurbomoleRead module, TurboReadBasis subroutine.'
                    write(stderr, *) '  Found basis function with l>lmax.'
                    stop
                end if
            end do
            abas(i)%ncart = ncart
            abas(i)%nsphe = nsphe
            rewind(inunit)
        end do
        close(inunit)
    end subroutine turboreadbasis


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboReadMos
    !
    ! DESCRIPTION:
    !> @brief Read the Turbomole molecular orbital files.
    !> @details
    !! Read the full set of molecular orbitals from a Turbomole $mos section into an array.
    !
    !> @param mo Molecular orbitals of the system.
    !----------------------------------------------------------------------------------------------
    subroutine turboreadmos(fnames, rhf, nmo, nbas, mo_c, mo_e)
        type(files), intent(inout) :: fnames !< List of file names.
        integer, intent(inout) :: rhf !< Restricted/unrestricted calculation.
        integer, intent(inout) :: nmo !< Number of molecular orbitals.
        integer, intent(inout) :: nbas !< Number of basis functions.
        real(dp), allocatable, intent(inout) :: mo_c(:, :, :) !< Molecular orbital coefficients.
        real(dp), allocatable, intent(inout) :: mo_e(:, :) !< Molecular orbital energies.
        integer :: inunit
        integer :: ios
        integer :: i
        integer :: j
        character(len=6), parameter :: key1 = '$scfmo'
        character(len=12), parameter :: key2a = '$uhfmo_alpha'
        character(len=11), parameter :: key2b = '$uhfmo_beta'
        character(len=:), allocatable :: key
        character(len=:), allocatable :: readformat
        integer :: ninline
        integer :: nreadmos
        integer :: cmo
        integer :: tempnbf

        if (rhf == 0) call turboreadrundim(fnames, rhf=rhf)
        if (nmo == 0) call turboreadrundim(fnames, nsphe=nmo)
        if (nbas == 0) call turboreadrundim(fnames, nsphe=nbas)

        if (.not. allocated(mo_c)) allocate(mo_c(nbas, nmo, rhf))
        if (.not. allocated(mo_e)) allocate(mo_e(nmo, rhf))

        do i = 1, rhf
            nreadmos = 0
            if (rhf == 1) key = key1
            if ((i == 1) .and. (rhf == 2)) key = key2a
            if ((i == 2) .and. (rhf == 2)) key = key2b

            if (fnames%q('mos', i) == 0) call turboreadfns(fnames)
            open(newunit=inunit, file=fnames%fn('mos', i), action='read')
            do
                call line%readline(inunit, comment='#', err=ios)
                if (ios /= 0) then
                    write(stderr, *) 'Error in TurbomoleRead module, TurboReadMos subroutine.'
                    write(stderr,'(2x,a)') 'Section molecular orbitals not found in file.'
                    stop
                end if
                if (index(line%str, key) == 1) then
                    if (index(line%str, 'format') /= 0) then
                        call line%parse('()') ! Extract the format string.
                        readformat = '('//line%args(2)//')'
                        ! To get the number of orbitals per line, the string is split after the 
                        ! first occurence of one of the fortran format specifiers for reals.
                        line%str = line%args(2)
                        call line%parse('def')
                        read(line%args(1), *) ninline
                    end if
                    do
                        call line%readline(inunit, comment='#')
                        if (index(line%str, '$') == 1) then
                            write(stderr, *) 'Error in TurbomoleRead module, &
                                             &TurboReadMos subroutine.'
                            write(stderr, *) '  Not all molecular orbitals found.'
                            stop
                        end if
                        call line%parse(' =')
                        read(line%args(1), *) cmo
                        if (cmo > nmo) then
                            do j = 1, nbas, ninline
                                read(inunit, *)
                            end do
                            cycle
                        end if
                        nreadmos = nreadmos + 1
                        read(line%args(4), *) mo_e(cmo, i)
                        read(line%args(6), *) tempnbf
                        if (tempnbf /= nbas) then
                            write(stderr, *) 'Error in TurbomoleRead module, &
                                             &TurboReadMos subroutine.'
                            write(stderr, *) '  Wrong number of basis functions in MO.'
                            stop
                        end if
                        do j = 1, nbas, ninline
                           call line%readline(inunit, comment='#')
                           read(line%str, readformat) mo_c(j:min(nbas,j+ninline-1), cmo, i)
                        end do
                        if (nreadmos == nmo) exit
                    end do
                    exit
                end if
            end do
            close(inunit)
        end do
    end subroutine turboreadmos


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboReadWF
    !
    ! DESCRIPTION:
    !> @brief Read single excitation coefficients from a Turbomole calculation.
    !> @details
    !! This subroutine only calls the appropriate read subroutine based on the type of calculation.
    !----------------------------------------------------------------------------------------------
    subroutine turboreadwf(fnames, opts, outopt, rhf, nocc, nvir, nstate, ndet, exen, wfa, wfb)
        use orthomod
        type(files), intent(inout) :: fnames !< List of file names.
        type(tmopts), intent(inout) :: opts !< Options for the turbomole calculation.
        integer, intent(in) :: outopt !< Requested format of TDDFT coefficients.
        integer, intent(inout) :: rhf !< Restricted/unrestricted calculation.
        integer, intent(inout) :: nocc(2)
        integer, intent(inout) :: nvir(2)
        integer, intent(inout) :: nstate
        integer, intent(inout) :: ndet(2)
        real(dp), allocatable :: exen(:)
        real(dp), allocatable :: wfa(:, :) !< Alpha coefficients.
        real(dp), allocatable :: wfb(:, :) !< Beta coefficients.
        integer :: supdim

        if (.not. opts%inited) call turboreadopts(fnames, opts)
        if (rhf == 0) call turboreadrundim(fnames, rhf = rhf)
        if (nstate == 0) then
            nstate = opts%nexstate
        else if (nstate > opts%nexstate) then
            write(stderr, *) 'Error in TurbomoleRead module, TurboReadWF subroutine.'
            write(stderr, '(2x,a,i0, i0)') 'Attempting to read ', nstate, ' excited states.'
            write(stderr, '(2x,a,i0)') 'Available excited states: ', opts%nexstate
            stop
        end if
        if (.not. allocated(exen)) allocate(exen(nstate))

        if (opts%dft) then
            supdim = 2
            if (opts%instab == 2) supdim = 1
            if (opts%functype < 3) supdim = 1
            if (outopt < 2) supdim = 1
            ndet(1) = supdim * nocc(1) * nvir(1)
            if (.not. allocated(wfa)) allocate(wfa(ndet(1), nstate))
            if (rhf == 2) then
                ndet(2) = supdim * nocc(2) * nvir(2)
                if (.not. allocated(wfb)) allocate(wfb(ndet(2), nstate))
            end if
            call tddftauxwf(fnames, opts, outopt, rhf, nocc, nvir, nstate, exen, wfa, wfb)
        else if (opts%ricc2) then
            ndet(1) = nocc(1) * nvir(1)
            if (rhf == 2) ndet(2) = nocc(2) * nvir(2)
            if (.not. allocated(wfa)) allocate(wfa(ndet(1), nstate))
            if ((rhf == 2) .and. (.not. allocated(wfb))) allocate(wfb(ndet(2), nstate))
            select case (opts%ricc2model)
            case(4)
                call adc2singex(rhf, nstate, wfa, wfb)
                call energy_exstates(fnames, exen, 'ADC(2)__')
            case default
                write(stderr, *) 'Error in TurbomoleRead module, TurboReadWF subroutine.'
                write(stderr, '(2x,a,i0)') 'Model not implemented: ', opts%ricc2model
                stop
            end select
        else
            write(stderr, *) 'Error in TurbomoleRead module, TurboReadWF subroutine.'
            write(stderr, '(2x,a)') 'Unknown method.'
            stop
        end if
       !call orth_gs(wfa)
       !if (rhf == 2) then
       !    call orth_gs(wfb)
       !    wfa = wfa / sqrt(2.0_dp)
       !    wfb = wfb / sqrt(2.0_dp)
       !end if
    end subroutine turboreadwf


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TDDFTAuxWF
    !
    ! DESCRIPTION:
    !> @brief Read the X and Y expansion coefficients of a TDDFT calculation.
    !> @details
    !! The format in which the output is given is determined by the outopt variable:
    !!    0 - X or X+Y        - Dimension: no * nv.
    !!    1 - sqrt(X^2 - Y^2) - Single electron excitation "coefficients". Dimension: no * nv.
    !!    2 - X+Y and X-Y     - Format of input files. Dimension: 2 * no * nv
    !! When dimension of super-tensorspace is 1 (LDA and GGA functionals or TDA approximation),
    !! output option 0 is always used.
    !----------------------------------------------------------------------------------------------
    subroutine tddftauxwf(fnames, opts, outopt, rhf, nocc, nvir, nstate, en, wfa, wfb)
        type(files), intent(inout) :: fnames !< List of file names.
        type(tmopts), intent(inout) :: opts !< Options from a turbomole control file.
        integer, intent(in) :: outopt !< Output option (see subroutine details).
        integer, intent(inout) :: rhf !< Restricted/unrestricted calculation.
        integer, intent(inout) :: nocc(2) !< Number of occupied orbitals.
        integer, intent(inout) :: nvir(2) !< Number of virtual orbitals.
        integer, intent(in) :: nstate !< Number of states.
        real(dp), allocatable :: en(:) !< Excitation energies.
        real(dp), allocatable :: wfa(:, :) !< Alpha coefficients.
        real(dp), allocatable :: wfb(:, :) !< Beta coefficients.

        integer :: state
        logical :: supdim1 !Dimension of super-tensor space is 1.
        integer :: dima !Alpha dimension.
        integer :: dimb !Beta dimension.
        integer :: tdim !Total dimension of super-tensor space.
        integer :: chkdim
        integer :: i
        integer :: cstate
        real(dp), allocatable :: tv1(:)
        integer :: inunit
        integer, parameter :: ninline = 4
        character(len=9), parameter :: lineform = '(4d20.14)'

        supdim1 = .false.
        if (opts%instab == 2) supdim1 = .true.
        if (opts%functype < 3) supdim1 = .true.

        ! Check dimensions.
        dima = nocc(1) * nvir(1)
        dimb = 0
        if (rhf == 2) dimb = nocc(2) * nvir(2)
        tdim = dima + dimb
        if (.not. supdim1) tdim = tdim * 2
        allocate(tv1(tdim))

        do state = 1, nstate
            open(newunit=inunit, file=fnames%fn('eigenpairs'), action='read')
     outer: do
                call line%readline(inunit, comment='#')
                if (index(line%str, '$tensor space dimension') == 1) then
                    call line%parse(' ')
                    read(line%args(4), *) chkdim
                    if (.not. supdim1) chkdim = chkdim * 2
                    if (tdim /= chkdim) then
                        write(stderr, *) 'Error in TurbomoleRead module, TDDFTAuxWF subroutine.'
                        write(stderr,'(a)') ' Error. Tensor space dimension mismatch:'
                        write(stderr, '(3x,a,i0)') 'Expected: ', tdim
                        write(stderr, '(3x,a,i0)') 'Read: ', chkdim
                    end if
                end if
                if (index(line%str, '$eigenpairs') == 1) then
     inner:         do 
                        call line%readline(inunit, comment='#')
                        if (index(line%str, 'eigenvalue') == 0) cycle ! Cycle until a state is found.
                        call line%parse(' =')
                        read(line%args(1), *) cstate
                        read(line%args(3), *) en(state)
                        if (cstate /= state) cycle ! Cyclue until correct state is found.
                        do i = 1, tdim, ninline
                            call line%readline(inunit, comment='#')
                            read(line%str, lineform) tv1(i:min(tdim, i + ninline - 1))
                        end do
                        exit outer
                    end do inner
                end if
            end do outer
            close(inunit)
    
            if (supdim1) then
                wfa(:, state) = tv1(1: dima)
                if (rhf == 2) wfb(:, state) = tv1(dima + 1 : tdim)
            else
                select case (outopt)
                case(0)
                    wfa(:, state) = tv1(1 : dima)
                    if (rhf == 2) wfb(:, state) = tv1(dima + 1 : dima + dimb)
                case(1)
                    wfa(:, state) = sqrt(abs(tv1(1 : dima) * tv1(dima + dimb + 1 : 2 * dima + dimb)))
                    do i = 1, dima
                        if (tv1(i) > 0) wfa(i, state) = - wfa(i, state)
                    end do
                    if (rhf == 2) then
                        wfb(:, state) = sqrt(abs(tv1(dima + 1 : dima + dimb) &
                                             & * tv1(2 * dima + dimb + 1 : tdim)))
                        do i = 1, dimb
                            if (tv1(dima + i) > 0) wfb(i, state) = - wfb(i, state)
                        end do
                    end if
                case(2)
                    wfa(1 : dima, state) = tv1(1 : dima)
                    wfa(dima + 1 : 2 * dima, state) = tv1(dima + dimb + 1 : 2 * dima + dimb)
                    if (rhf == 2) then
                        wfb(1 : dimb, state) = tv1(dima + 1 : dima + dimb)
                        wfb(dimb + 1 : 2 * dimb, state) = tv1(2 * dima + dimb + 1 : tdim)
                    end if
                end select
            end if
        end do
    end subroutine tddftauxwf



    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ADC2SingEx
    !
    ! DESCRIPTION:
    !> @brief Read the singles coefficients from a ricc2 ADC(2) calculation.
    !> @details
    !! The coefficients of singly excited determinants are read from the CCRE0 files.
    !> @note Coefficients of doubly excited determinants are not saved in ricc2 calculations. 
    !!       This means the wavefunctions read by this module are approximate.
    !----------------------------------------------------------------------------------------------
    subroutine adc2singex(rhf, nstate, wfa, wfb)
        integer, intent(inout) :: rhf !< Restricted/unrestricted calculation.
        integer, intent(in) :: nstate !< Number of states.
        real(dp), allocatable :: wfa(:, :) !< Alpha coefficients.
        real(dp), allocatable :: wfb(:, :) !< Beta coefficients.

        integer :: inunit
        logical :: check
        integer :: state
        integer, parameter :: lenstk = 4
        character(len=lenstk) :: statekey
        character(len=:), allocatable :: inputfile
        integer :: dima
        integer :: dimb
        character(len=3) :: dummybin
        real(dp) :: norm

        ! Check dimensions.
        dima = size(wfa, 1)
        if (rhf == 2) dimb = size(wfb, 1)

        ! Check input file.
        do state = 1, nstate
            statekey = '----'
            write(statekey(lenstk-int(log10(real(state))):lenstk), '(i0)') state
            inputfile = 'CCRE0-1--1'//statekey
            inquire(file=inputfile, exist=check)
            if (.not. check) then
                write(stderr,*) 'Error in TurbomoleRead module, ADC2Singles subroutine.'
                write(stderr,*) ' File (', inputfile,') not found.'
                stop
            end if
            
            ! Read input file.
            open(newunit=inunit, file=inputfile, form='UNFORMATTED', action='READ')
                read(unit=inunit) dummybin(1:3)
                read(unit=inunit) wfa(1 : dima, state)
                if (rhf == 2) read(unit=inunit) wfb(1 : dimb, state)
            close(inunit)
            
            norm = sum(wfa(:, state) * wfa(:, state))
            if (rhf == 2) then
                norm = norm + sum(wfb(:, state) * wfb(:, state))
                wfb(:, state) = wfb(:, state) / sqrt(norm)
            end if
            wfa(:, state) = wfa(:, state) / sqrt(norm)
        end do

    end subroutine adc2singex


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboReadEn
    !
    ! DESCRIPTION:
    !> @brief Read the energies from a turbomole calculation.
    !> @details
    !! This subroutine only calls the appropriate read subroutines based on the type of calculation.
    !! When available, the excitation energies are read from the "spectrum" file, but can also be
    !! read from the files containing the $eigenpairs section for TDDFT or from the exstate file
    !! containing the appropriate $excitation_energies keyword for ricc2 calculations.
    !
    !> @param opts Type containing the options from a turbomole control file.
    !> @param en Energies of the electronic states.
    !----------------------------------------------------------------------------------------------
    subroutine turboreaden(fnames, opts, en)
        type(files), intent(inout) :: fnames !< List of file names.
        type(tmopts), intent(inout) :: opts
        real(dp), intent(inout), allocatable :: en(:)

        if (.not. allocated(en)) allocate(en(opts%nexstate + 1))
        
        if (opts%hf .or. opts%dft) then
            call energy_energy(fnames, en(1), 'SCF')
        else if (opts%ricc2) then
            select case (opts%ricc2model)
            case (2)
                call energy_energy(fnames, en(1), 'MP2')
            case (4)
                call energy_cc(fnames, en(1), 'MP2')
            case (5)
                call energy_cc(fnames, en(1), 'CC2')
            case (6)
                call energy_cc(fnames, en(1), 'CCSD')
            end select
        end if


        if (opts%nexstate > 0) then
            if (opts%spectrum) then
                call energy_spectrum(fnames, en(2:))
                en(2:) = en(2:) + en(1)
            else if (opts%hf .or. opts%dft) then
                call energy_eigenpairs(fnames, opts, en(2:))
                en(2:) = en(2:) + en(1)
            else if (opts%ricc2) then
                select case (opts%ricc2model)
                case (4)
                    call energy_exstates(fnames, en(2:), 'ADC(2)__')
                case (5)
                    call energy_exstates(fnames, en(2:), 'CC2_____')
                case (6)
                    call energy_exstates(fnames, en(2:), 'CCSD____')
                end select
                en(2:) = en(2:) + en(1)
            end if
        end if
    end subroutine turboreaden


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: energy_energy
    !
    ! DESCRIPTION:
    !> @brief Read the last SCF energy from the energy file.
    !> @param gsen Ground state energy.
    !----------------------------------------------------------------------------------------------
    subroutine energy_energy(fnames, gsen, model)
        type(files), intent(inout) :: fnames !< List of file names.
        real(dp), intent(out) :: gsen
        character(len=*), intent(in) :: model
        logical :: check
        integer :: inunit
        real(dp) :: dtmp

        inquire(file=fnames%fn('energy'), exist=check)
        if (.not. check) then
            write(stderr,*) 'Error in TurbomoleRead module, energy_SCF subroutine.'
            write(stderr,'(3x,a)') ' File not found:'
            write(stderr,'(5x,a)') fnames%fn('energy')
            stop
        end if
        open(newunit=inunit, file=fnames%fn('energy'), action='read')
outer:  do
            call line%readline(inunit, comment='#')
            call line%parse(' =')
            if (line%args(1) == '$energy') then
inner:          do
                    call line%readline(inunit, comment='#')
                    if (index(line%str,'$') == 1) exit outer
                    call line%parse(' ')
                    read(line%args(2), *) gsen
                    if (model == 'MP2') then
                        read(line%args(5), *) dtmp
                        gsen = gsen + dtmp
                    end if
                end do inner
            end if
        end do outer
        close(inunit)
    end subroutine energy_energy

    

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: energy_CC
    !
    ! DESCRIPTION:
    !> @brief Read the ground state energy from the restart.cc file.
    !> @param gsen Ground state energy.
    !> @param model Name of the wave function model to search for.
    !----------------------------------------------------------------------------------------------
    subroutine energy_cc(fnames, gsen, model)
        type(files), intent(inout) :: fnames !< List of file names.
        real(dp), intent(out) :: gsen
        character(len=*), intent(in) :: model
        logical :: check
        integer :: inunit
        integer :: ios

        inquire(file=fnames%fn('restartcc'), exist=check)
        if (.not. check) then
            write(stderr,*) 'Error in TurbomoleRead module, energy_cc subroutine.'
            write(stderr,'(3x,a)') ' File not found:'
            write(stderr,'(5x,a)') fnames%fn('restartcc')
            stop
        end if
        open(newunit=inunit, file=fnames%fn('restartcc'), action='read')
        do
            call line%readline(inunit, comment='#', err=ios)
            if (ios /= 0) then
                write(stderr, *) 'Error in TurbomoleRead module, energy_cc subroutine.'
                write(stderr,'(3x,a, a, a)') '$energy_', model, ' section not found in file:'
                write(stderr,'(5x,a)') fnames%fn('restartcc')
                stop
            end if
            call line%parse(' =')
            if (line%args(1) == '$energy_'//model) then
                read(line%args(2), *) gsen
                exit
            end if
        end do
        close(inunit)
    end subroutine energy_cc


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: energy_spectrum
    !
    ! DESCRIPTION:
    !> @brief Read the last set of excitation energies from a turbomole spectrum file.
    !
    !> @param en Excitation energies.
    !----------------------------------------------------------------------------------------------
    subroutine energy_spectrum(fnames, en)
        type(files), intent(inout) :: fnames !< List of file names.
        real(dp), intent(out) :: en(:)
        logical :: check
        integer :: inunit
        integer :: ios
        integer :: i
        integer :: nline

        inquire(file=fnames%fn('spectrum'), exist=check)
        if (.not. check) then
            write(stderr,*) 'Error in TurbomoleRead module, energy_spectrum subroutine.'
            write(stderr,'(3x,a)') ' File not found:'
            write(stderr,'(5x,a)') fnames%fn('spectrum')
            stop
        end if

        nline = 0
        open(newunit=inunit, file=fnames%fn('spectrum'), action='read')
        do
            call line%readline(inunit, comment='#', err=ios)
            if (ios /= 0) exit
            nline = nline + 1
        end do
        close(inunit)

        open(newunit=inunit, file=fnames%fn('spectrum'), action='read')
        do i = 1, nline - size(en)
            call line%readline(inunit, comment='#')
        end do
        do i = 1, size(en)
            call line%readline(inunit, comment='#')
            call line%parse(' ')
            read(line%args(1), *) en(i)
        end do
        close(inunit)
        
    end subroutine energy_spectrum
 

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: energy_eigenpairs
    !
    ! DESCRIPTION:
    !> @brief Read the excited state energies from the $eigenpairs section.
    !> @param opts Type containing the options from a turbomole control file.
    !> @param en Excitation energies.
    !----------------------------------------------------------------------------------------------
    subroutine energy_eigenpairs(fnames, opts, en)
        type(files), intent(inout) :: fnames !< List of file names.
        type(tmopts), intent(inout) :: opts
        real(dp), intent(out) :: en(:)
        integer :: inunit
        logical :: check
        integer :: nstate
        integer :: cstate
        logical, allocatable :: readen(:)

        if (fnames%q('eigenpairs', 0) == 0) call turboreadopts(fnames, opts)
        inquire(file=fnames%fn('eigenpairs'), exist=check)
        if (.not. check) then
            write(stderr,*) 'Error in TurbomoleRead module, energy_eigenpairs subroutine.'
            write(stderr,'(3x,a)') ' File not found:'
            write(stderr,'(5x,a)') fnames%fn('eigenpairs')
            stop
        end if
 
        nstate = size(en)
        allocate(readen(nstate))
        readen = .false.

        open(newunit=inunit, file=fnames%fn('eigenpairs'), action='read')
outer:  do
            call line%readline(inunit, comment='#')
            if (index(line%str, '$eigenpairs') /= 1) cycle
inner:      do
                call line%readline(inunit, comment='#')
                call line%parse(' =')
                if (line%args(2) /= 'eigenvalue') cycle
                read(line%args(1), *) cstate
                if (cstate > nstate) cycle
                read(line%args(3), *) en(cstate)   
                readen(cstate) = .true.
                if (all(readen)) exit outer
            end do inner
        end do outer
        close(inunit)
    end subroutine energy_eigenpairs


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: energy_exstates
    !
    ! DESCRIPTION:
    !> @brief Read the $excitation_energies_ section from a ricc2 calculation.
    !
    !> @param en Excitation energies.
    !> @param model Name of the wave function model to search for.
    !----------------------------------------------------------------------------------------------
    subroutine energy_exstates(fnames, en, model)
        type(files), intent(inout) :: fnames !< List of file names.
        real(dp), intent(out) :: en(:)
        character(len=*), intent(in) :: model

        integer :: inunit
        integer :: ios
        integer :: i
        character(len=:), allocatable :: sectionkey
        integer :: nstate
        integer :: checkns

        sectionkey = '$excitation_energies_'//model//'1^a___'
        if (fnames%q(sectionkey, 0) == 0) call turboreadfns(fnames)

        nstate = size(en)
        open(newunit=inunit, file=fnames%fn(sectionkey), action='read')
        do
            call line%readline(inunit, comment='#', err=ios)
            if (ios /= 0) then
                write(stderr, *) 'Error in TurbomoleRead module, TurboReadExciEn subroutine.'
                write(stderr,'(3x,a,a,a)') 'Section ', sectionkey, ' not found in file:'
                write(stderr,'(5x,a)') fnames%fn(sectionkey)
                stop
            end if
            if (index(line%str, sectionkey) == 1) then
                call line%parse(' =')
                read(line%args(5), *) checkns
                if (checkns < nstate) then
                    write(stderr, *) 'Error in TurbomoleRead module, TurboReadExciEn subroutine.'
                    write(stderr,'(3x,a,a,a)') 'Number of states mismatch in ', sectionkey,'.'
                    stop
                end if
                do i = 1, nstate
                    call line%readline(inunit, comment='#')
                    call line%parse(' ')
                    read(line%args(2), *) en(i)
                end do
                exit
            end if
        end do
        close(inunit)
    end subroutine energy_exstates


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboReadOS
    !
    ! DESCRIPTION:
    !> @brief Read the oscillator strengths from a turbomole calculation.
    !> @details
    !! This subroutine only calls the appropriate read subroutines based on the type of calculation.
    !! When available, the oscillator strenths are read from the "spectrum" file, but for ricc2
    !! calculations they can also be read from the "exstates" file containing the appropriate
    !! "tranprop_..." keyword.
    !
    !> @param opts Type containing the options from a turbomole control file.
    !> @param os Oscillator strengths of the electronic states.
    !----------------------------------------------------------------------------------------------
    subroutine turboreados(fnames, opts, os)
        type(files), intent(inout) :: fnames !< List of file names.
        type(tmopts), intent(inout) :: opts
        real(dp), intent(out), allocatable :: os(:)

        if (.not. allocated(os)) allocate(os(opts%nexstate))
        if (opts%spectrum) then
            call oscillator_spectrum(fnames, os)
        else if (opts%ricc2 .and. opts%ricc2spectrum) then
            select case (opts%ricc2model)
            case (4)
                call oscillator_exstates(fnames, os, 'ADC(2)____')
            case (5)
                call oscillator_exstates(fnames, os, 'CC2_______')
            end select
        end if
    end subroutine turboreados


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: oscillator_spectrum
    !
    ! DESCRIPTION:
    !> @brief Read the last set of oscillator strengths from a turbomole spectrum file.
    !
    !> @param os Oscillator strengths of the electronic states.
    !----------------------------------------------------------------------------------------------
    subroutine oscillator_spectrum(fnames, os)
        type(files), intent(inout) :: fnames !< List of file names.
        real(dp), intent(out) :: os(:)
        logical :: check
        integer :: inunit
        integer :: ios
        integer :: i
        integer :: nline

        inquire(file=fnames%fn('spectrum'), exist=check)
        if (.not. check) then
            write(stderr,*) 'Error in TurbomoleRead module, oscillator_spectrum subroutine.'
            write(stderr,'(3x,a)') ' File not found:'
            write(stderr,'(5x,a)') fnames%fn('spectrum')
            stop
        end if

        nline = 0
        open(newunit=inunit, file=fnames%fn('spectrum'), action='read')
        do
            call line%readline(inunit, comment='#', err=ios)
            if (ios /= 0) exit
            nline = nline + 1
        end do
        close(inunit)

        open(newunit=inunit, file=fnames%fn('spectrum'), action='read')
        do i = 1, nline - size(os)
            call line%readline(inunit, comment='#')
        end do
        do i = 1, size(os)
            call line%readline(inunit, comment='#')
            call line%parse(' ')
            read(line%args(2), *) os(i)
        end do
        close(inunit)
    end subroutine oscillator_spectrum
    

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: oscillator_exstates
    !
    ! DESCRIPTION:
    !> @brief Read the oscillator strenths from the $tranprop section of a turbomole calculation.
    !
    !> @param os Oscillator strengths of the electronic states.
    !> @param model Name of the wave function model to search for.
    !----------------------------------------------------------------------------------------------
    subroutine oscillator_exstates(fnames, os, model)
        type(files), intent(inout) :: fnames !< List of file names.
        real(dp), intent(out) :: os(:)
        character(len=*), intent(in) :: model

        integer :: inunit
        integer :: i
        character(len=42) :: sectionkey
        character(len=6) :: tempstr
        integer :: nstate
        real(dp) :: dx
        real(dp) :: dy
        real(dp) :: dz

        sectionkey = '$tranprop_'//model//'1^a_____0_to_1^a______'

        nstate = size(os)
        do i = 1, nstate
            write(tempstr, '(i0)') i
            sectionkey(43-len(trim(adjustl(tempstr))):) = trim(adjustl(tempstr))
            if (fnames%q(sectionkey, 0) == 0) call turboreadfns(fnames)
            open(newunit=inunit, file=fnames%fn(sectionkey), action='read')
 outer:         do
                    call line%readline(inunit, comment='#')
                    if (index(line%str, sectionkey) /= 1) cycle
 inner:             do
                        call line%readline(inunit, comment='#')
                        if (index(line%str, '$end') == 1) then
                            write(stderr, *) 'Error in TurbomoleRead module, &
                                              &oscillator_exstates subroutine.'
                            write(stderr,'(3x,a,a)') 'Section: ', sectionkey
                            write(stderr,'(3x,a)') 'Keyword "diplen: (strength)" not found.'
                            stop
                        end if
                        if (index(line%str, 'diplen: (strength)') /=1) cycle
                        call line%readline(inunit, comment='#')
                        call line%parse(' =')
                        read(line%args(2),*) dx
                        read(line%args(4),*) dy
                        read(line%args(6),*) dz
                        os(i) = dx + dy + dz
                        exit outer
                    end do inner
                end do outer
            close(inunit)
        end do
    end subroutine oscillator_exstates


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TurboReadGrad
    !
    ! DESCRIPTION:
    !> @brief Read the last gradient from a turbomole $grad section.
    !----------------------------------------------------------------------------------------------
    subroutine turboreadgrad(fnames, natom, grad)
        type(files), intent(inout) :: fnames !< List of file names.
        integer, intent(inout) :: natom !< Number of atoms.
        real(dp), allocatable, intent(inout) :: grad(:, :) !< Gradient.
        logical :: check
        integer :: i
        integer :: inunit

        if (fnames%q('gradient', 0) == 0) call turboreadfns(fnames)
        inquire(file=fnames%fn('gradient'), exist=check)
        if (.not. check) then
            write(stderr,*) 'Error in TurbomoleRead module, TurboReadGrad subroutine.'
            write(stderr,'(3x,a,a)') ' File not found: ', fnames%fn('gradient')
            stop
        end if
        if (.not. allocated(grad)) allocate(grad(3, natom))

        open(newunit=inunit, file=fnames%fn('gradient'), action='read')
 outer: do
            call line%readline(inunit, comment='#')
            call line%parse(' ')
            if (line%args(1) == '$grad') then
 inner:         do
                    call line%readline(inunit, comment='#')
                    if (index(line%str,'$') == 1) exit outer
                    do i = 1, natom
                        call line%readline(inunit, comment='#')
                    end do
                    do i = 1, natom
                        call line%readline(inunit, comment='#')
                        read(line%str, *) grad(:, i)
                    end do
                end do inner
            end if
        end do outer
        close(inunit)
    end subroutine turboreadgrad
   

    subroutine turboreadhess(fnames, hess)
        type(files), intent(inout) :: fnames !< List of file names.
        real(dp), allocatable, intent(inout) :: hess(:, :) !< Hessian.
        integer :: natom !< Number of atoms.
        integer :: dof !< Degrees of freedom.
        integer, parameter :: ninline = 5
        integer :: i, j, du
        integer :: inunit
        logical :: check

        if (fnames%q('hessian', 0) == 0) call turboreadfns(fnames)
        inquire(file=fnames%fn('hessian'), exist=check)
        if (.not. check) then
            write(stderr,*) 'Error in TurbomoleRead module, TurboReadHessian subroutine.'
            write(stderr,'(3x,a,a)') ' File not found: ', fnames%fn('hessian')
            stop
        end if
        if (.not. allocated(hess)) then
            natom = 0
            call turboreadrundim(fnames, natom=natom)
            dof = 3 * natom
            allocate(hess(dof, dof))
        else
            dof = size(hess, 1)
        end if

        open(newunit=inunit, file=fnames%fn('hessian'), action='read')
 outer: do
            call line%readline(inunit, comment='#')
            if (trim(line%str) /= '$hessian (projected)') cycle outer
            do i = 1, dof
                do j = 1, dof, ninline
                write(*,*) i,j, min(dof, j+ninline-1)
                    call line%readline(inunit, comment='#')
                    read(line%str, *) du, du, hess(i, j:min(dof, j+ninline-1))
                end do
            end do
            exit outer
        end do outer
        close(inunit)
    end subroutine turboreadhess


end module turbomolereadmod
