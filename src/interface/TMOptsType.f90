module tmoptstype

    type tmopts
        logical :: inited = .false.

        character(len=:), allocatable :: symmetry

        logical :: hf = .false.
        logical :: rij = .false.

        logical :: dft = .false.
        integer :: instab = 0
        character(len=:), allocatable :: func
        integer :: functype = 0

        logical :: ricc2 = .false.
        integer :: ricc2model = 0
        logical :: ricc2spectrum = .false.

        logical :: spectrum = .false.
        logical :: pcharges = .false.
        integer :: nexstate = 0
        integer :: gradient = -1
    end type tmopts

end module tmoptstype
