!--------------------------------------------------------------------------------------------------
!> @mainpage Wave function overlap calculation.
!> Program for calculating the wave function overlap matrix between wave functions from two
!! electronic structure calculations.
!--------------------------------------------------------------------------------------------------
! PROGRAM: TM_Smat
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date May, 2017
!
! DESCRIPTION: 
!> @brief Compute wave function overlap matrix.
!> @details 
!> @details Reads the atomic and molecular orbitals and single excitation wave function coefficients
!! from two Turbomole calculations. The AO overlap matrix is calculated by the OneElOp subroutine 
!! and used to calculate the MO overlap matrix. The CIS_Olap_Full subroutine calculates the wave 
!! function overlaps using the MO overlap matrix to generate singly excited determinants.
!! The two calculations can have different atomic orbital basis sets or different geometries.
!! If one calculation is restricted while the other is unrestricted, the restricted orbitals are 
!! treated as both alpha and beta spin orbitals.
!--------------------------------------------------------------------------------------------------
program tm_smat
    use fortutils
    use filestype
    use tmoptstype
    use qminfotype
    use turbomolereadmod
    use turbomolewritemod
    use oneelopmod
    use mooverlapmod
    use cisoverlapmod
    use sparsemod
    use sparsewfmod
    use write_txt_mod
    implicit none
    
    integer :: narg
    character(len=1000) :: temp
    character(len=:), allocatable :: pwd
    character(len=:), allocatable :: dir1
    character(len=:), allocatable :: dir2
  
    type(files) :: fn1
    type(files) :: fn2
    type(tmopts) :: opt1
    type(tmopts) :: opt2
    type(qminfo) :: qm1
    type(qminfo) :: qm2

    integer :: i, s, o, v, outunit
    integer, allocatable :: ref(:)
    real(dp), allocatable :: smat(:, :)
    character(len=1), parameter :: detc(0:3) = ['e','a','b','d']
    character(len=80) :: detfmt
    


    call getcwd(temp)
    pwd = trim(adjustl(temp))

    narg = command_argument_count()
    if (narg /= 2) stop 'Need two directory names.'
    call get_command_argument(1, temp)
    dir1 = trim(adjustl(temp))
    call get_command_argument(2, temp)
    dir2 = trim(adjustl(temp))

    ! Read first directory.
    call chdir(dir1)
    call turboreadall(fn1, opt1, qm1)
    call chdir(pwd)

    ! Read second directory.
    call chdir(dir2)
    call turboreadall(fn2, opt2, qm2)
    call chdir(pwd)

    call write_txt('mo1', qm1%mo_c(:,qm1%nmo_fo(1)+1:,:))
    call write_txt('mo2', qm2%mo_c(:,qm1%nmo_fo(1)+1:,:))
    call write_txt('trans1', qm1%ao_c2s)
    call write_txt('trans2', qm2%ao_c2s)

    call write_txt('wfa1', reshape(qm1%wf(1)%c, [qm1%nmo_av(1), qm1%nmo_ao(1), qm1%nexst]))
    call write_txt('wfa2', reshape(qm2%wf(1)%c, [qm2%nmo_av(1), qm2%nmo_ao(1), qm2%nexst]))
    if (qm1%rhf == 2) call write_txt('wfb1', reshape(qm1%wf(2)%c, [qm1%nmo_av(2), qm1%nmo_ao(2), qm1%nexst]))
    if (qm2%rhf == 2) call write_txt('wfb2', reshape(qm2%wf(2)%c, [qm2%nmo_av(2), qm2%nmo_ao(2), qm2%nexst]))


    ! Calculate molecular orbital overlaps.
    call oneelop(qm1%ao_c, qm2%ao_c, [0,0,0], qm1%ao_c2s, qm2%ao_c2s, smat)
    call write_txt('ao_ovl', smat)

    detfmt = ''
    write(detfmt, '(a,i0,a)') '(', qm1%nmo, 'a, 1000(2x, e22.15))'
    call occ_nums_to_vec(qm1%nmo, qm1%nmo_o, ref)
    open(newunit=outunit, file='dets.1', action='write')
    write(outunit, '(3(i0,1x))') qm1%nexst, qm1%nmo, sum(qm1%ndet)
    do s = 1, qm1%rhf
        i = 0
        do o = qm1%nmo_fo(s) + 1, qm1%nmo_o(s)
            do v = qm1%nmo_o(s) + 1, qm1%nmo_av(s) + qm1%nmo_o(s)
                i = i + 1
                write(outunit, detfmt) detc(ovec_move(ref, s, o, v)), qm1%wf(s)%c(i, :)
            end do
        end do
    end do
    close(outunit)

    detfmt = ''
    write(detfmt, '(a,i0,a)') '(', qm2%nmo, 'a, 1000(2x, e22.15))'
    call occ_nums_to_vec(qm2%nmo, qm2%nmo_o, ref)
    open(newunit=outunit, file='dets.2', action='write')
    write(outunit, '(3(i0,1x))') qm2%nexst, qm2%nmo, sum(qm2%ndet)
    do s = 1, qm2%rhf
        i = 0
        do o = qm2%nmo_fo(s) + 1, qm2%nmo_o(s)
            do v = qm2%nmo_o(s) + 1, qm2%nmo_av(s) + qm2%nmo_o(s)
                i = i + 1
                write(outunit, detfmt) detc(ovec_move(ref, s, o, v)), qm2%wf(s)%c(i, :)
            end do
        end do
    end do
    close(outunit)

!-----------------------------------------------------------------

contains

    subroutine occ_nums_to_vec(n, o, ovec)
        integer, intent(in) :: n
        integer, intent(in) :: o(2)
        integer, allocatable, intent(out) :: ovec(:)

        if (allocated(ovec)) deallocate(ovec)
        allocate(ovec(n), source = 0)
        ovec(1:o(1)) = ovec(1:o(1)) + 1
        ovec(1:o(2)) = ovec(1:o(2)) + 2
    end subroutine occ_nums_to_vec

    function ovec_move(ref, s, o, v) result(new)
        integer, intent(in) :: ref(:)
        integer, intent(in) :: s
        integer, intent(in) :: o
        integer, intent(in) :: v
        integer, allocatable :: new(:)
        allocate(new, source = ref)
        new(o) = new(o) - s
        new(v) = new(v) + s
    end function ovec_move



end program tm_smat
