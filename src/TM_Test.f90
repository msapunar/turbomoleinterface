!--------------------------------------------------------------------------------------------------
!> @mainpage Test Turbomole Interface
!> Program for testing the subroutines in the modules TurbomoleReadMod and TurbomoleWriteMod.
!--------------------------------------------------------------------------------------------------
! PROGRAM: Test
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date May, 2017
!
! DESCRIPTION: 
!> @brief Test the subroutines in the modules TurbomoleReadMod and TurbomoleWriteMod.
!> @details 
!! Main program file, to be called from a directory containing a Turbomole calculation.
!! Reads all information about the Turbomole calculation from the files in the directory using
!! subroutines from TurbomoleReadMod and writes the read information to stdout and to interface_*
!! files using subroutines from TurbomoleWriteMod.
!--------------------------------------------------------------------------------------------------
program testinterface
    use fortutils
    use constants
    use filestype
    use tmoptstype
    use qminfotype
    use turbomolereadmod
    use turbomolewritemod
    use sparsemod
    use sparsewfmod
    implicit none

    type(files) :: fnames
    type(tmopts) :: opts
    type(qminfo) :: qm
    type(sparsev), allocatable :: cis(:, :)
    real(dp), allocatable :: olap(:)
    integer :: i, j
    integer :: a, b

    call turboreadall(fnames, opts, qm)
    call gensparsewf(qm%rhf, qm%nexst, qm%ndet, qm%wf, 0.9_dp, cis)
    if (opts%hf) then
        if (opts%rij) then
            write(*,'(a)') 'Method: RI-HF'
        else
            write(*,'(a)') 'Method: HF'
        end if
    else if (opts%dft) then
        if (opts%rij) then
            write(*,'(a)') 'Method: RI-DFT'
        else
            write(*,'(a)') 'Method: DFT'
        end if
        write(*,'(2x,a,a)') 'Functional: ', opts%func
        select case(opts%functype)
        case(1)
            write(*,'(2x,a)') 'Functional type: LDA'
        case(2)
            write(*,'(2x,a)') 'Functional type: GGA'
        case(3)
            write(*,'(2x,a)') 'Functional type: MGGA'
        case(4)
            write(*,'(2x,a)') 'Functional type: Hybrid'
        case(5)
            write(*,'(2x,a)') 'Functional type: Double-hybrid'
        end select
    else if (opts%ricc2) then
        write(*,'(a)') 'Method: RICC2'
        select case(opts%ricc2model)
        case(4)
            write(*,'(2x,a)') 'Model: ADC(2)'
        case(5)
            write(*,'(2x,a)') 'Model: CC2'
        case(6)
            write(*,'(2x,a)') 'Model: CCSD'
        end select
    end if

    ! Write geometry to a new coord file. Should be identical to the original $coord section.
    call turbowritecoord('interface_coord', qm%axyz, qm%asym)
    ! Write atom basis to a new basis file. The number formats are not identical, and the basis 
    ! functions are sorted, but the file should be usable by future turbomole calculations and
    ! give the same results.
    call turbowritebasis('interface_basis', qm%abas)
    ! Write molecular orbitals to a new file(s). Should be identical to the original file(s).
    call turbowritemos('interface_mosa', 'interface_mosb', qm%rhf, qm%nmo, qm%naos, qm%mo_c, qm%mo_en)

    if (qm%rhf == 1) write(*, '(a)') 'Restricted.'
    if (qm%rhf == 2) write(*, '(a)') 'Unrestricted.'
    write(*, '(2x,a)') 'Number of occupied/virtual orbitals: '
    write(*, '(4x,i0,1x,i0)') qm%nmo_o(1), qm%nmo_v(1)
    if (qm%rhf == 2) write(*, '(4x,i0,1x,i0)') qm%nmo_o(2), qm%nmo_v(2)
    write(*, '(2x,a)') 'Number of active occupied/virtual orbitals: '
    write(*, '(4x,i0,1x,i0)') qm%nmo_ao(1), qm%nmo_av(1)
    if (qm%rhf == 2) write(*, '(4x,i0,1x,i0)') qm%nmo_ao(2), qm%nmo_av(2)


    write(*,'(a, 2x, e21.13)') 'Ground state energy: ', qm%en(1)
    do i = 1, qm%nexst
        write(*, '(2x,a,i0)') 'Excited state ', i
        write(*, '(4x,a,f20.10 )') 'Excitation energy (eV): ', qm%exen(i) * eh_eV
        if (opts%spectrum .or. opts%ricc2spectrum) then
            write(*,'(4x, a, f20.10)') 'Oscillator strength: ', qm%exos(i)
        end if
        write(*, '(4x,a)') 'Main contributions:'
        if (qm%rhf == 2) write(*,'(6x,a)') 'Alpha:'
        do j = 1, cis(1, i)%n
            a = int(cis(1, i)%i(j) / qm%nmo_av(1)) + qm%nmo_fo(1) + 1
            b = mod(cis(1, i)%i(j), qm%nmo_av(1)) + qm%nmo_o(1)
            write(*, '(6x,i3,2x,i3,f12.5)') a, b, cis(1, i)%v(j)**2
        end do
        if (qm%rhf == 2) then
            write(*,'(6x,a)') 'Beta:'
            do j = 1, cis(2, i)%n
                a = int(cis(2, i)%i(j) / qm%nmo_av(2)) + qm%nmo_fo(2) + 1
                b = mod(cis(2, i)%i(j), qm%nmo_av(2)) + qm%nmo_o(2)
                write(*, '(6x,i3,2x,i3,f12.5)') a, b, cis(2, i)%v(j)**2
            end do
        end if
    end do

    if (qm%nexst > 0) then
        write(*, '(a)') 'Wave function overlaps: '
        allocate(olap(qm%nexst))
    end if
    do i = 1, qm%nexst
        do j = 1, qm%nexst
            olap(j) = dot_product(qm%wf(1)%c(:, i), qm%wf(1)%c(:, j))
        end do
        if (qm%rhf == 2) then
            do j = 1, qm%nexst
                olap(j) = olap(j) + dot_product(qm%wf(1)%c(:, i), qm%wf(1)%c(:, j))
            end do
        end if
        write(*, '(2x,1000e23.15)') olap
    end do

!   ! Read the calculated gradient.
!   inquire(file=fnames%fn('gradient'), exist=check)
!   if (check) then
!       call turboreadgrad(fnames, opts%gradient, mol)
!       write(*, '(a,i0)') 'Gradient of state: ', opts%gradient
!       do i = 1, size(mol)
!           write(*, '(2x,3e21.13)') mol(i)%gra(1:3)
!       end do
!   end if

end program testinterface
