!--------------------------------------------------------------------------------------------------
!> @mainpage Wave function overlap calculation.
!> Program for calculating the wave function overlap matrix between wave functions from two
!! electronic structure calculations.
!--------------------------------------------------------------------------------------------------
! PROGRAM: TM_Smat
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date May, 2017
!
! DESCRIPTION: 
!> @brief Compute wave function overlap matrix.
!> @details 
!> @details Reads the atomic and molecular orbitals and single excitation wave function coefficients
!! from two Turbomole calculations. The AO overlap matrix is calculated by the OneElOp subroutine 
!! and used to calculate the MO overlap matrix. The CIS_Olap_Full subroutine calculates the wave 
!! function overlaps using the MO overlap matrix to generate singly excited determinants.
!! The two calculations can have different atomic orbital basis sets or different geometries.
!! If one calculation is restricted while the other is unrestricted, the restricted orbitals are 
!! treated as both alpha and beta spin orbitals.
!--------------------------------------------------------------------------------------------------
program tm_smat
    use fortutils
    use filestype
    use tmoptstype
    use qminfotype
    use turbomolereadmod
    use turbomolewritemod
    use oneelopmod
    use mooverlapmod
    use cisoverlapmod
    use sparsemod
    use sparsewfmod
    implicit none
    
    integer :: narg
    character(len=1000) :: temp
    character(len=:), allocatable :: pwd
    character(len=:), allocatable :: dir1
    character(len=:), allocatable :: dir2
  
    type(files) :: fn1
    type(files) :: fn2
    type(tmopts) :: opt1
    type(tmopts) :: opt2
    type(qminfo) :: qm1
    type(qminfo) :: qm2
    type(sparsev), allocatable :: swf1(:, :)
    type(sparsev), allocatable :: swf2(:, :)

    integer :: i
 integer :: j,b
    integer :: s
    integer :: rhf
    real(dp), allocatable :: smat(:, :)
    real(dp), allocatable :: cscmat(:, :, :)
    real(dp), allocatable :: tcscmat(:, :, :)
    real(dp), allocatable :: omat(:, :)
    real(dp), allocatable :: tmpmat(:, :)
    integer, allocatable :: row2col(:)
    logical :: paoo = .false. !< Print atomic orbital overlaps.
    logical :: pmoo = .false. !< Print molecular orbital overlaps.
    logical :: pwfo = .true. !< Print wave function overlaps.
    logical :: opt_trunwf = .false. !< Truncate wave functions.
    logical :: opt_assign = .false. !< Assign and reorder wf overlap matrix beofre printing.
    logical :: opt_phasem = .false. !< Assign and match phases of wf overlap matrix.


    call getcwd(temp)
    pwd = trim(adjustl(temp))

    narg = command_argument_count()
    do i = 1, narg 
        call get_command_argument(i, temp)
        select case(trim(temp))
        case('-ao')
            paoo = .true.
        case('-mo')
            pmoo = .true.
        case('-wf')
            pwfo = .true.
     !  case('-truncate')
     !      opt_trunwf = .true.
     !  case('-phase')
     !      opt_phasem = .true.
     !  case('-assign')
     !      opt_assign = .true.
        end select
        if (i == narg - 1) dir1 = trim(adjustl(temp))
        if (i == narg) dir2 = trim(adjustl(temp))
    end do

    ! Read first directory.
    call chdir(dir1)
    call turboreadall(fn1, opt1, qm1)
    call chdir(pwd)

    ! Read second directory.
    call chdir(dir2)
    call turboreadall(fn2, opt2, qm2)
    call chdir(pwd)

    ! Calculate molecular orbital overlaps.
    rhf = max(qm1%rhf, qm2%rhf)
    call oneelop(qm1%ao_c, qm2%ao_c, [0,0,0], qm1%ao_c2s, qm2%ao_c2s, smat)
    if (paoo) then
        do i = 1, qm1%naos
            write(stdout, '(10000(e22.15,2x))') smat(i, :)
        end do
        stop
    end if
    call mooverlap(qm1%mo_c, qm2%mo_c, smat, tcscmat)
    if (pmoo) then
        do s = 1, rhf
            do i = 1, qm1%nmo
                write(stdout, '(10000(e22.15,2x))') tcscmat(i, :, s)
            end do
        end do
        stop
    end if
    ! Remove frozen orbitals from MO overlap matrix.
    call reducecsc(qm1%mo_omask, qm2%mo_omask, qm1%mo_amask, qm2%mo_amask, tcscmat, cscmat)

!-----------------------------------------------------------------
    open(newunit=narg, file='cscmat')
    write(narg, *) rhf, qm1%nmo_a, qm2%nmo_a
    do b = 1, rhf
        do i = 1, qm2%nmo_a
            write(narg, '(10000(x,e23.17))') cscmat(:, i, b)
        end do
    end do
    close(narg)
    open(newunit=narg, file='wf1')
    write(narg, '(2(x,i0))') qm1%nexst, qm1%rhf
    do i = 1, qm1%rhf
        write(narg, '(2(x,i0))') qm1%nmo_ao(i), qm1%nmo_av(i)
    end do
    do i = 1, qm1%nexst
        write(narg, '(10000(x,e23.17))') qm1%exen(i)
        do j = 1, qm1%rhf
            do b = 1, qm1%nmo_ao(j)
                write(narg, '(10000(x,e23.17))') qm1%wf(j)%c((b-1)*qm1%nmo_av(j)+1:b*qm1%nmo_av(j) ,i)
            end do
        end do
    end do
    close(narg)
    open(newunit=narg, file='wf2')
    write(narg, '(2(x,i0))') qm2%nexst, qm2%rhf
    do i = 1, qm2%rhf
        write(narg, '(2(x,i0))') qm2%nmo_ao(i), qm2%nmo_av(i)
    end do
    do i = 1, qm2%nexst
        write(narg, '(10000(x,e23.17))') qm2%exen(i)
        do j = 1, qm2%rhf
            do b = 1, qm2%nmo_ao(j)
                write(narg, '(10000(x,e23.17))') qm2%wf(j)%c((b-1)*qm2%nmo_av(j)+1:b*qm2%nmo_av(j) ,i)
            end do
        end do
    end do
    close(narg)
    stop
!-----------------------------------------------------------------
    allocate(omat(1:qm1%nst, 1:qm2%nst))
    omat = 0.0_dp
    if (opt_trunwf) then
    !   call gensparsewf(qm1%rhf, qm1%nexst, qm1%ndet, qm1%wf, 0.9_dp, swf1)
    !   call gensparsewf(qm2%rhf, qm2%nexst, qm2%ndet, qm2%wf, 0.9_dp, swf2)
    !   call cis_olap_trun(cscmat, qm1%nmo_ao, qm1%nmo_av, qm2%nmo_av, qm1%rhf, swf1, qm2%rhf,     &
    !   &                  swf2, omat)
    else
        call cis_olap_full(cscmat, qm1%nmo_ao, qm1%nmo_av, qm2%nmo_av, qm1%wf, qm2%wf, omat)
    end if

    ! Output overlap matrix:
    if (pwfo) then
        do i = 1, qm1%nst
            write(stdout, '(10000(e22.15,2x))') omat(i, :)
        end do
    end if

end program tm_smat
