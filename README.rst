.. _example:

##########################
Turbomole Interface Module
##########################

.. sidebar:: Software Technical Information

  Language
    Fortran 2008

  Licence
    MIT license (MIT)

  Documentation Tool
    Doxygen.

.. contents:: :local:


.. Add technical info as a sidebar and allow text below to wrap around it

Purpose of Module
_________________

The Turbomole Interface Module contains a set of subroutines used to read the results of
calculations performed using the Turbomole package for *ab initio* electronic structure
calculations. It also contains subroutines for writing files
in a format usable by the package.

Routines are included to:

- read and write molecular coordinates
- read and write basis sets
- read and write molecular orbitals
- read energies of electronic states
- read oscillator strengths of excited states
- read gradients
- read wave function coefficients

Most subroutines were written and only tested for TDDFT and ADC(2) calculations.

Background Information
______________________

The module was originally developed to interface the Turbomole package with a nonadiabatic
surface-hopping dynamics program. The TM_NAD program reads the status of the dynamics calculation
and the geometry of the system, runs a turbomole calculation and outputs the new energies and
gradients. It also computes and outputs the overlap between the previous and current (approximate)
ADC(2) or TDDFT wave functions. The TM_Smat program can be used to just compute the wave function
overlap for two calculations.


Testing
_______

An example program (tm_test.exe) used for testing the interface is included in the module. To test
the interface run it in a directory containing a Turbomole calculation. The program was tested for
Turbomole v7.0.


Source Code
___________

The source code is available at: Git_


.. _Git: https://bitbucket.org/msapunar/turbomoleinterface

